/***********************************************************************
* Program:
*    Test 2, Flipping a coin
*    Brother Helfrich, CS124
* Author:
*    The Key
* Summary: 
*    Perform a simple coin-flipping experiment.  The user will specify the
*    number of flips
************************************************************************/

#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

int promptNumberOfTrials();
int performExperiment(int number);
bool flippedHeads();
void display(int numberHeads, int numberTails);

/**********************************************************************
 * MAIN
 * Prompt the user for the number of trials, perform the experiment, and
 * display the results
 ***********************************************************************/
int main(int argc, char **argv)
{
   // this code is necessary to set up the random number generator. If
   // your program uses a random number generator, you will need this
   // code. Otherwise, you can safely delete it.  Note: this must go in main()
   srand(argc == 1 ? time(NULL) : (int)argv[1][1]);
   
   // how many trials
   int numberTrials = promptNumberOfTrials();

   // perform the experiment
   int numberHeads = performExperiment(numberTrials);

   // display the results.  Since we only kept track of the number
   // of heads, we need to do some math to figure the number of tails
   display(numberHeads, numberTrials - numberHeads);
   
   return 0;
}

/*************************************************
 * PROMPT NUMBER OF TRIALS
 * Prompt the user for the number of trials. This is
 * a standard prompt() function.
 ************************************************/
int promptNumberOfTrials()
{
   int number;
   cout << "How many coin flips for this experiment: ";
   cin  >> number;
   return number;
}

/**********************************************
 * PERFORM EXPERIMENT
 * Flip those coins, Mike
 *********************************************/
int performExperiment(int number)
{
   // how many heads have we gotten so far?
   int numHeads = 0;
   // (Init: WHat needs to happe to begin; Bool when does loop term; 
   // Incrament is there a counter or other state which changes as the loop
   // executes?)
   // go through 'number' flips
   for (int i = 0; i < number; i++)
   // What occurs inside the loop
      if (flippedHeads())           // if it is heads...
         numHeads++;                // ... then add one to numHeads

   // return and report
   return numHeads;
}

/********************************************
 * FLIPPED HEADS
 * Was the coin heads?
 *******************************************/
bool flippedHeads()
{
   return rand() % 2 == 0;          // even means heads
}

/*******************************************
 * DISPLAY
 * Display the results
 ******************************************/
void display(int numberHeads, int numberTails)
{
   cout << "There were " << numberHeads << " heads.\n";
   cout << "There were " << numberTails << " tails.\n";
}
