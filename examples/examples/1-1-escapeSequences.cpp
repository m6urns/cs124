/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use escape sequences
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main().  Just one function for a simple program
 ***********************************************************************/
int main()
{
   // Display the instructions
   cout << "The escape sequences are:\n";
   
   // Display the newline
   cout << "\tNewline  \\n\n";

   // Display the Tab
   cout << "\tTab      \\t\n";      

   // Display the Slash
   cout << "\tSlash    \\\\\n";

   // The Single Quote
   cout << "\tSQuote   \\'\n";

   // The Double Quote
   cout << "\tDQuote   \\\"\n";

   return 0;
}

