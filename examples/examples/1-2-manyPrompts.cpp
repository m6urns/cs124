/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to declare many types of variables and
 *      accept input from the user
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main(): Just one function for now.
 ***********************************************************************/
int main()
{
   // declare the variables
   char  name[256];
   int   age;
   float gpa;
   char  letterGrade;
   
   // prompt for name
   cout << "What is your first name: ";
   cin  >> name;
   
   // prompt for age
   cout << "What is your age: ";
   cin  >> age;

   // prompt for GPA
   cout << "What is your GPA: ";
   cin  >> gpa;
   
   // prompt for expected grade
   cout << "What grade do you hope to get in CS 124: ";
   cin  >> letterGrade;

   // configure the display to show GPAs: one digit of accuracy
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(1);
   
   // display the results
   cout << "\t" << name
        << ", you are " << age
        << " with a " << gpa
        << " GPA.  You will get an " << letterGrade
        << ".\n";

   return 0;
}

