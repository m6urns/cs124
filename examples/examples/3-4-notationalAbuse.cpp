/***********************************************************************
 * This demo program is designed to:
 *      The square-bracket notation of arrays is actually a short-hand
 *      for the *( + ) notation. 
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * MAIN: How to melt brains (hehehe)
 ***********************************************************************/
int main()
{
   int value[5] = { 2001, 1888, 1984, 1492, 1776};

   cout << "    value[3] = " << value[3]     << endl;  // normal notation
   cout << "*(value + 3) = " << *(value + 3) << endl;  // pointer notation
   cout << "*(3 + value) = " << *(3 + value) << endl;  // commutative addition
   cout << "    3[value] = " << 3[value]     << endl;  // hehehe
   return 0;
}
