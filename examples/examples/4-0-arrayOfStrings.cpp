/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate working with a 2-dimensional array of characters,
 *      otherwise known as an array of strings.
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************
 * PROMPT NAMES
 * Prompt the user for his or her name
 **********************************************************/
void promptNames(char names[][256])           // the column dimension must 
{                                             //     be the buffer size
   // prompt for name (first, middle, last)
   cout << "What is your first name?  ";       
   cin  >> names[0];                          // passing 1 instance of array
   cout << "What is your middle name? ";      //    of names to CIN
   cin  >> names[1];                          // Note that the data type is
   cout << "What is your last name?   ";      //    a pointer to a char,
   cin  >> names[2];                          //    what CIN expects
}

/**********************************************************************
 * MAIN
 * Just a silly demo program
 ***********************************************************************/
int main()
{
   char names[3][256];                  // arrays of strings are multi-
                                        //    dimensional arrays of chars

   // fill the array
   promptNames(names);                  // pass the entire array
   
   // first name
   cout << "First name: ";              // this is an array of characters
   cout << names[0] << endl;

   // middle initial
   cout << "Middle initial: ";
   cout << names[1][0] << endl;         // the first letter of second string

   // loop through the names for output
   cout << "Fill name: ";
   for (int i = 0; i < 3; i++)
      cout << names[i] << ' ';
   cout << endl;   

   return 0;
}

