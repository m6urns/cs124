/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to work with string objects
 ************************************************************************/

#include <iostream>
#include <string>    // needed whenever we declare a variable of type string
using namespace std;

/**********************************************************************
 * MAIN: A simple demo of the string class
 ***********************************************************************/
int main()
{
   string firstName;   // no []s required.  The string takes care
   string lastName;    //    of the buffer

   // cin and cout work as you would expect with the string class
   cout << "What is your name (first last): ";
   cin  >> firstName >> lastName;

   // Append ", " after last name so "Young" becomes "Young, ".
   // To do this, we will use the += operator
   lastName += ", ";

   // Create a new string containing the first and last name so
   //        "Brigham" "Young"
   // becomes
   //        "Young, Brigham"
   // To do this, we will use the + operator to combine
   // two strings and the = operator to assign the results to a new string
   string fullName = lastName + firstName;

   // display the results of our nifty creation
   cout << fullName << endl;
   
   return 0;
}

