/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to read a list of numbers from a file
 ************************************************************************/

#include <iostream>
#include <fstream>    // needed for all file I/O programs
using namespace std;

/**************************************************
 * SUM DATA
 * Read a list of grades from a file and display the sum
 **************************************************/
int sumData(char fileName[])
{
   // open the file
   ifstream fin(fileName);       // open the file
   if (fin.fail())
      return 0;                  // I could display an error message...
   
   // read the data
   int data;                     // always need a variable to store the data
   int sum = 0;                  // don't forget to initialize the variable
   while (fin >> data)           // read while there is not an error
      sum += data;               // do the work
   
   // close the file and return the sum
   fin.close();
   return sum;                   // return the sum
}

/**********************************************************************
 * main(): Simple driver program
 ***********************************************************************/
int main()
{
   // get the filename
   char fileName[256];
   cout << "What is the filename: ";
   cin  >> fileName;

   // call the sum function and display the results
   cout << "The sum is: "
        << sumData(fileName)
        << endl;
   return 0;
}

