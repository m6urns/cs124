/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate the table-lookup method of using an array to find
 *      the number of points corresponding to a given finishing place
 *      in a race
 ************************************************************************/

#include <iostream>
using namespace std;

/*****************************************************
 * PROMPT FOR POINTS
 * Prompt the user for all the places he finished during
 * this season's races and return the number of
 * corresponding points
 *****************************************************/
int promptForPoints()
{
   int points = 0;                        // start the season with zero
   int breakdown[4][2] =
   {
      {1, 5},                             // 1st gets 5 points
      {2, 3},                             // 2nd gets 3 
      {3, 2},
      {4, 1}
   };

   // loop through the 10 races in the season
   for (int cRace = 0; cRace < 10; cRace++) // "cRace for" "count race"
   {
      // get the place for a given race
      int place;
      cout << "What was your place? ";
      cin  >> place;

      // add the points to the total
      for (int cPlace = 0; cPlace < 4; cPlace++) // loop through all places
         if (breakdown[cPlace][0] == place)      // if place in table match
            points += breakdown[cPlace][1];      // assign the ponits
   }
   
   return points;
}

/**********************************************************************
 * MAIN: prompt the user for his place and display the number of
 *       points he earned.
 ***********************************************************************/
int main()
{
   // find the number of points
   int points = promptForPoints();

   // display the number of points
   cout << "You earned "
        << points
        << " points this season\n";

   return 0;
}

