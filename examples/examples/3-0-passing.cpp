/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate passing arrays of strings as a parameter
 ************************************************************************/

#include <iostream>
using namespace std;

/*******************************************
 * DISPLAY NAMES
 * Display all the names in the passed list
 ******************************************/
void displayNames(const char names[][256], int num)  // second [] has the size
{
   cout << "All " << num << " names on the list:\n";
   for (int i = 0; i < num; i++)               // same as with other arrays
      cout << '\t' << names[i] << endl;        // access each individual string
}

/*******************************************
 * DISPLAY NAME
 * Display a single name
 ******************************************/
void displayName(const char name[])  // no NUM variable, no value in the[]s
{
   cout << "One single name:\n";
   cout << '\t' << name << endl;
}


/******************************************
 * DISPLAY LETTER
 * Display a single letter
 ******************************************/
void displayLetter(char letter)
{
   cout << "One single letter:\n";
   cout << '\t' << letter << endl;
}

/**********************************************************************
 * MAIN: Just a function to exercise displayNames, displayName, displayLetter
 ***********************************************************************/
int main()
{
   char fullName[3][256] =
   {
      "Thomas",
      "Edwin",
      "Ricks"
   };

   char word[256] = "BYU-Idaho";

   char letter = 'C';

   //
   // Test displayNames
   //
   
   // The only way we can call displayNames is when we pass a list of strings
   displayNames(fullName, 3);

   //
   // Test displayName
   //

   // I can pass one instance of the list of names:
   displayName(fullName[2]);

   // I can pass a single string
   displayName(word);

   // I can pass a string literal
   displayName("Vikings");

   //
   // Test displayLetter
   //

   // Here we will pass the first letter of the second word
   displayLetter(fullName[2][0]);

   // Next, we will pass the fifth letter from the word
   displayLetter(word[4]);

   // Next, we will pass the single letter
   displayLetter(letter);

   // Finally, we will pass a single character literal
   displayLetter('K');

   return 0;
}

