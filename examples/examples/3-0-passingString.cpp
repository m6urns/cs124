/***********************************************************************
 * This demo program is designed to:
 *      Show how to pass text to a function, either as input or for output
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/********************************************
 * DISPLAY NAME
 * Display a user.s name on the screen
 ********************************************/
void displayName(const char lastName[], // no number inside brackets!
                 bool isMale) 
{
   if (isMale)
      cout << "Brother ";
   else
      cout << "Sister ";
   cout << lastName;                    // treated like any other string
   return;
}

/******************************************
 * GET NAME
 * Prompt the user for this last name
 ******************************************/
void getName(char lastName[])         // Even though this is pass-by-reference
{                                     //     there is no & in the parameter
   cout << "What is your last name? ";
   cin  >> lastName;
   return;                             // Do not use the return mechanism
}                                      //     for passing arrays

/*********************************************
 * MAIN
 * Driver function for displayName and getName
 *********************************************/
int main()
{
   char name[256];                         // this buffer is 256 characters
   getName(name);                          // no []s used when passing arrays

   displayName(name,    true  /*isMale*/); // again, no []s
   displayName("Smith", false /*isMale*/); // we can also pass a string literal
                                           // this buffer is not 256 chars!
   return 0;
}
