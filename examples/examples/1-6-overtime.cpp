/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate IF statements and parameter passing.  All this is
 *      used to demonstrate what your paycheck should look like when
 *      you take time-and-a-half overtime into account.
 ************************************************************************/

#include <iostream>
using namespace std;

double getHourlyWage();
double getHoursWorked();
double computePay(double hourlyWage, double hoursWorked);

/**********************************************************************
 * main():  Prompt for the wage and the hours worked.  Then display
 *          the pay 
 ***********************************************************************/
int main()
{
   // get the data
   double hourlyWage  = getHourlyWage();
   double hoursWorked = getHoursWorked();

   // output. No variables here; send the data directly to COUT
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);
   cout << "Pay: $ " << computePay(hourlyWage, hoursWorked) << endl;      

   return 0;
}

/*********************************
 * GET HOURLY WAGE
 *     Prompt the user for his wage.  Kinda personal.  A
 *     FLOAT would work here because most make less
 *     than 100,000.00 an hour.
 *********************************/
double getHourlyWage()
{
   // prompt
   cout << "What is your hourly wage? ";
   
   // get data
   double wage;
   cin  >> wage;
   return wage;
}

/**********************************
 * GET HOURS WORKED
 *     Note that we could work part of an hour
 *     so a float or a double would work here.
 *     Again, a double is over-kill
 **********************************/
double getHoursWorked()
{
   //  prompt
   cout << "How many hours did you work? ";
   
   // get data
   double hours;
   cin >> hours;
   return hours;
}

/************************************************
 * COMPUTE PAY
 *    Compute the user's pay taking time-and-a-half
 *    overtime into account.
 ***********************************************/
double computePay(double hourlyWage, double hoursWorked)
{
   double pay;

   // regular rate
   if (hoursWorked <= 40.0)
      pay = hoursWorked * hourlyWage;                 // regular rage

   // overtime rate
   else
      pay = (40.0 * hourlyWage) +                     // first 40 are normal
         ((hoursWorked - 40.0) * (hourlyWage * 1.5)); // balance overtime
   
   return pay;
}
