/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate a simple event-controlled loop.
 *
 *      This program will continue to prompt the user for his GPA
 *      until a valid range is reached.  A driver program will verify it.
 ************************************************************************/

#include <iostream>
#include <cassert>  
using namespace std;

float getGPA();

/**********************************************************************
 * main(): A driver program for getGPA
 ***********************************************************************/
int main()
{
   // get the value
   float gpa = getGPA();
   
   // display it
   cout.setf(ios::fixed);     // who would have a GPA in scientific notation
   cout.setf(ios::showpoint); // we usually show the decimal point for GPAs
   cout.precision(1);         // most people display a GPA to 1 digit
   cout << "GPA: " << gpa << endl;

   return 0;
}

/*************************************************************
 * GET GPA
 * Keep prompting the user for a GPA until he give a valid one
 *    WHILE gpa > 4.0 or gpa < 0.0
 *         PROMPT for GPA
 *         GET gpa
 *************************************************************/
float getGPA()
{
   float gpa = -1.0;  // any value outside the expected range will do

   // loop until a valid value is reached
   while (gpa > 4.0 || gpa < 0.0)
   {
      cout << "Please enter your GPA (0.0 <= gpa <= 4.0): ";
      cin  >> gpa;
   }

   // return with the loot
   assert(gpa <= 4.0 && gpa >= 0.0); // paranoia will destroy-ya
   return gpa;
}
