/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use a switch-case statement to choose between
 *      more than two options
 ************************************************************************/

#include <iostream>
using namespace std;

void display(int score, int par);

/**********************************************************************
 * MAIN: Simple driver program to test the display function
 ***********************************************************************/
int main()
{
   // prompt for the golf score
   int golfScore;
   cout << "What is your golf score? ";
   cin  >> golfScore;
   
   // prompt for the par for the hole
   int par;
   cout << "What is par for the hole? ";
   cin  >> par;
   
   // display the score
   display(golfScore, par);

   return 0;
}

/*****************************************************
 * DISPLAY
 * Translate the golfer performance into a "bogie",
 * "eagle", or "par"
 *****************************************************/
void display(int score, int par)
{
   switch (score - par)
   {
      case 2:
         cout << "You got a double bogie\n";
         break;
      case 1:
         cout << "You got a bogie\n";
         break;

      case 0:
         cout << "You got par for the hole\n";
         break;

      case -1:
         cout << "You got a birdie\n";
         break;
      case -2:
         cout << "You got an eagle\n";
         break;

      default:
         cout << "Your score was quite unusual\n";
   }
   
   return;
}
