/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to test a function using a driver program.  This
 *      will make it easier to find bugs.
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

double computeTax(double incomeMonthly);

/*****************************************************
 * MAIN
 *    A simple driver program for computeTax()
 *****************************************************/
int main()
{
   // get the income
   double income;              // the inputs to the function being
   cout << "Income: ";         //    tested is gathered directly from
   cin  >> income;             //    the user and sent to the function

   // call the function and display the results
   cout << "computeTax(" << income << ") == "  // what we are sending...
        << computeTax(income)                  // what the output is
        << endl;
   
   return 0;
}




/*******************************************************
 * COMPUTE TAX
 *    Determine the tax on the given monthly income
 *****************************************************/
double computeTax(double incomeMonthly)
{
   assert(incomeMonthly >= 0.0);
   double incomeYearly = incomeMonthly * 12.0;
   double taxYearly;

   // 10% tax bracket
   if (incomeYearly < 15100.00)
      taxYearly = incomeYearly * 0.10;

   // 15% tax bracket
   else if (incomeYearly < 61300.00)
      taxYearly = (incomeYearly - 15100.00) * 0.15 + 1510.00;

   // 25% tax bracket
   else if (incomeYearly < 123700.00)
      taxYearly = (incomeYearly - 61300.00) * 0.25 + 8400.00;

   // 28% tax bracket
   else if (incomeYearly < 188450.00)
      taxYearly = (incomeYearly - 123700.00) * 0.28 + 24040.00;

   // 33% tax bracket
   else if (incomeYearly < 336550.00)
      taxYearly = (incomeYearly - 188450.00) * 0.33 + 42170.00;

   // 35% tax bracket
   else
      taxYearly = (incomeYearly - 336550.00) * 0.35 + 91043.00;

   // monthly tax
   double taxMonthly = taxYearly / 12.0;
   assert(taxMonthly >= 0);
   assert(taxMonthly <= incomeMonthly);
   return taxMonthly;
}

