/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to loop through an array. This will be done
 *      by copying the data in one array into another.
 ************************************************************************/

#include <iostream>    // for CIN and COUT
#include <cassert>     // for ASSERTs
using namespace std;

/**********************************************************************
 * main(): Simple demo program for looping through an array
 ***********************************************************************/
int main()
{
   // Declare the variables
   const int SIZE = 10;                   // this is a CONST so we are safe
   int listDestination[SIZE];             // copy data to here
   int listSource[SIZE];                  // copy data from here
   assert(SIZE == sizeof(listSource) / sizeof(listSource[0]));
   assert(SIZE == sizeof(listDestination) / sizeof(listDestination[0]));
   
   // Fill the list
   cout << "Please enter " << SIZE << " values.\n";
   for (int i = 0; i < SIZE; i++)
   {
      assert(i >= 0 && i < SIZE);         // yes, I am paranoid
      cout << "\t# " << i + 1 << ": ";    // humans start counting at 1
      cin  >> listSource[i];              // computers start at 0
   }
   
   // Copy the list
   for (int i = 0; i < SIZE; i++)         // same loop we use for all arrays
   {
      assert(i >= 0 && i < SIZE);         // very, very paranoid
      listDestination[i] = listSource[i]; // copy one at a time
   }
   
   // Display the results
   cout << "Your list was: "
        << listDestination[0];            // display the first item
   for (int i = 1; i < SIZE; i++)         // standard loop skipping the 1st
   {
      assert(i > 0 && i < SIZE);          // did I mention I am paranoid?
      cout << ", " << listDestination[i]; // all the rest have a , before
   }
   cout << endl;

   return 0;
}

