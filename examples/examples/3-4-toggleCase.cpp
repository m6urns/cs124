/***********************************************************************
 * This demo program is designed to:
 *      How to walk through a string using a FOR loop.  In this case, we
 *      will convert uppercase to lowercase (and vice-versa).
 *      This version of the program will use the pointer notation instead
 *      of the array index notation.
 ************************************************************************/

#include <iostream>
#include <cassert>
#include <cctype>
using namespace std;

void prompt(char text[], int max);
void convert(char text[]);
void display(const char text[]);

/**********************************************************************
 * MAIN
 * Just a simple driver program for our convert() function
 ***********************************************************************/
int main()
{
   // get the user's text
   char text[256];
   prompt(text, sizeof(text) / sizeof(text[0]));

   // convert uppercase to lowercase and vice-versa
   convert(text);
   
   // display the results
   display(text);

   return 0;
}

/**************************************************
 * PROMPT
 * Ask the user for his text
 **************************************************/
void prompt(char text[], int max)
{
   assert(max > 0);
   cout << "Please enter your text: ";
   cin.getline(text, max);
}

/*************************************************
 * CONVERT
 * tHIS PROGRAM WILL CONVERT uPPERCASE TO lOWERCASE
 * AND vICE-vERSA.
 *************************************************/
void convert(char text[])
{
   for (char * p = text; *p; p++)   // second standard FOR loop
      if (isupper(*p))              // check character with *p
         *p = tolower(*p);          // assign to *p
      else
         *p = toupper(*p);
}

/***************************************************
 * DISPLAY
 * Exactly the same functionality as
 *     cout << text << endl;
 **************************************************/
void display(const char text[])
{
   for (const char * p = text; *p; p++)  // second standard FOR loop
      cout << *p;   

   cout << endl;
}
