/***********************************************************************
 * This demo program is designed to:
 *      Compute the Child Tax Credit
 ************************************************************************/

#include <iostream>
using namespace std;

double getIncome();
int    getNumChildren();
bool   qualify(double);

/**********************************************************************
 * main(): Get the income and the number of children and, based on that,
 *         determine if the user qualifies for the child tax credit.
 ***********************************************************************/
int main()
{
   // get the income
   double income = getIncome();

   // get the number of children
   int numChildren = getNumChildren();
   
   // display the message
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);
   cout << "Child tax credit: $";
   if (qualify(income))
      cout << 1000.00 * (float)numChildren << endl;
   else
      cout << 0.00 << endl;

   return 0;
}

/*************************************
 * GET INCOME
 *    Get the user's income.  Note that we need to use
 *    a DOUBLE here because the FLOAT only has 7 digits of
 *    accuracy.  This will be great if you make $99,999.99 a
 *    year, but not if you make $100,000.00
 *************************************/
double getIncome()
{
   // prompt
   cout << "What is your income: ";

   // get the value
   double income;
   cin >> income;
   return income;
}

/*******************************************
 * GET NUM CHILDREN
 *   Needs to be an integer. Noone has -3.1 children!
 *******************************************/
int getNumChildren()
{
   // prompt
   cout << "How many children? ";

   // get the value
   int num; // probably should be:  unsigned int num;
   cin >> num;
   return num;
}

/*****************************************
 * QUALIFY
 *    Does the user qualify for the tax credit?
 *    This will return a BOOL because you either
 *    qualify or you don't!
 *****************************************/
bool qualify(double income)
{   
   return (income <= 110000.00);
}
