/***********************************************************************
 * This demo program is designed to:
 *      How to allocate a multi-dimensional array
 *      try:
 *         How many rows? 40
 *         How many columns? 78
 *         What is the filename? 4-1-allocateImage.txt
 ************************************************************************/

#include <iostream>
#include <fstream>
#include <cassert>
using namespace std;

char * allocate(int numRow, int numCol);
void display(const char * image, int numRow, int numcol);
void promptForSize(int & numRow, int & numCol);
void getFilename(char fileName[]);
bool readFile(char * image,
              const char fileName[],
              const int numRow,
              const int numCol);

/**********************************************************************
 * MAIN
 * Call the functions and hold the data
 ***********************************************************************/
int main()
{
   // get the number of items in the array from the user
   int numRow;
   int numCol;
   promptForSize(numRow, numCol);
   
   // allocate the memory
   char * image = allocate(numRow, numCol);

   // read the image from a file
   char fileName[256];
   getFilename(fileName);
   if (!readFile(image, fileName, numRow, numCol))
   {
      cerr << "Error: Unable to open file "
           << fileName
           << " for reading\n";
      return 1;
   }

   // display the image
   display(image, numRow, numCol);

   // make like a tree
   delete [] image;                   // never forget to release the memory
   return 0;
}

/*********************************
 * ALLOCATE
 * Grab the memory, returning NULL if
 * anything went wrong
 *********************************/
char * allocate(int numRow, int numCol)
{
   assert(numRow > 0 && numCol > 0);

   // we allocate a 1-dimensional array and do the
   // two dimensional math ourselves
   char * image = new(nothrow) char[numRow * numCol];
   if (!image)
   {
      cout << "Unable to allocate "
           << numRow * numCol * sizeof(char)
           << " bytes for a "
           << numCol << " x " << numRow
           << " image\n";
      return NULL;
   }
   return image;
}

/*********************************
 * DISPLAY
 * Display the image. This is ASCII-art
 * so it is not exactly "High resolution"
 *********************************/
void display(const char * image, int numRow, int numCol)
{
   // paranoia
   assert(image);
   assert(numRow > 0 && numCol > 0);

   // display the grid
   for (int row = 0; row < numRow; row++)       // two dimensional loop, first
   {                                            //    the rows, then
      for (int col = 0; col < numCol; col++)    //    the columns
         cout << image[row * numCol + col];     // do the [] math ourselves
   }
   cout << endl;
}

/*****************************************
 * PROMPT FOR SIZE
 * We need to get two items from the user:
 * the row and the column size. Because of
 * this, we cannot just return the answer, it
 * must be send back to the caller by-reference
 ****************************************/
void promptForSize(int & numRow, int & numCol)
{
   // number of rows
   cout << "How many rows? ";
   cin  >> numRow;

   // number of columns
   cout << "How many columns? ";
   cin  >> numCol;
}

/***************************************
 * GET FILENAME
 * Prompt for filename
 ***************************************/
void getFilename(char fileName[])
{
   cout << "What is the filename? ";
   cin  >> fileName;
}

/**************************************
 * READ FILE
 * Read the data from a file
 *************************************/
bool readFile(char * image,
              const char fileName[],
              const int numRow,
              const int numCol)
{
   // open the file
   ifstream fin(fileName);
   if (fin.fail())
      return false;

   // read the actual data
   for (int r = 0; r < numRow; r++)
      for (int c = 0; c < numCol; c++)
         image[r * numCol + c] = fin.get(); // do the 2d work ourselves

   
   // close the file
   fin.close();
   return true;  // success!
}
