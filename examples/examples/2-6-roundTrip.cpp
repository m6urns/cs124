/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to save data to a file and load data from a file.
 *      We call this process "round-tripping" data through the file.
 ************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

#define FILENAME "2-6-roundTrip.txt"

float getBalance();
float getChange();
void writeBalance(float balance);

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // get the balance from the file
   float balance = getBalance();
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);
   cout << "Account balance: $"
        << balance
        << endl;
   
   // change the balance
   float change = getChange();
   balance += change;
   cout << "New balance: $"
        << balance
        << endl;
   
   // write the new balance to the file
   writeBalance(balance);
   
   return 0;
}

/************************************
 * GET BALANCE
 * Fetch the balance from the file.  Note
 * that if the file does not exist or if
 * there is a problem, then return 0.0
 ************************************/
float getBalance()
{
   // open the file
   ifstream fin(FILENAME);
   if (fin.fail())
      return 0.0;

   // read the data
   float value = 0.0;
   fin >> value;
   if (fin.fail())
      return 0.0;
   
   // close and return the data
   fin.close();
   return value;
}

/******************************
 * GET CHANGE
 * Prompt the user for how much the
 * balance has changed
 *****************************/
float getChange()
{
   float change;
   cout << "Change: $";
   cin  >> change;
   return change;
}

/********************************
 * WRITE BALANCE
 * Write the new balance to the file
 *******************************/
void writeBalance(float balance)
{
   // open the file for writing
   ofstream fout(FILENAME);
   if (fout.fail())
   {
      cout << "ERROR! Unable to write balance to the file\n";
      return;
   }
   
   // write the data
   fout.precision(2);
   fout.setf(ios::fixed);
   fout.setf(ios::showpoint);
   fout << balance << endl;
   
   // close up shop
   fout.close();
}
