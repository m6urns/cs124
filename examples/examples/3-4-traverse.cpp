/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to traverse a c-string using a pointer
 ************************************************************************/

#include <iostream>
using namespace std;

/************************************
 * DISPLAY
 * Display the passed text one character
 * at a time using a pointer
 ***********************************/
void display(const char * text)
{
   // second standard for loop
   for (const char * p = text; *p; p++)
      cout << '\t' << *p << endl;
}


/**********************************************************************
 * MAIN: Simple driver program to exercise our display() function
 ***********************************************************************/
int main()
{
   // prompt for a string
   char text[256];
   cout << "Please enter the text: ";
   cin.getline(text, 256);

   // display the string
   display(text);

   return 0;
}

