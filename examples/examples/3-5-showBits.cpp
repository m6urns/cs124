/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use simple bit-wise operators by displaying
 *      the bits associated with an integer
 ************************************************************************/

#include <iostream>
using namespace std;

/********************************************************
 * DISPLAY BITS
 * Display the bits corresponding to an integer
 ********************************************************/
void displayBits(unsigned int value)
{
   unsigned int mask = 0x80000000;          // only the left-most bit is set

   for (int bit = 31; bit >= 0; bit--)      // go through all 32 bits
   {
      cout << ((mask & value) ? '1' : '0'); // check the current bit
      mask = mask >> 1;                     // shift the mask by one space
   }

   cout << endl;
}

/**********************************************************************
 * MAIN: Driver proram for displayBits()
 ***********************************************************************/
int main()
{
   // prompt the user for the number
   unsigned int value;
   cout << "Please enter a positive integer: ";
   cin  >> value;
   
   // display the bits
   displayBits(value);

   return 0;
}

