/***********************************************************************
 * This demo program is designed to:
 *      Multi-dimensional arrays of integers.  Both the single-dimensional
 *      way where the programmer must do the coordinate conversion, and the
 *      two-dimensional way where the compiler does the conversion.
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/******************************************
 * FILL ARRAY 2D
 * Fill a two-dimensional array with the multiplication tables
 ******************************************/
void fillArray2D(int grid[][4], int numRow)
{
   for (int row = 0; row < numRow; row++)
      for (int col = 0; col < 4; col++)
         grid[row][col] = row * col;
}

/**********************************************
 * FILL ARRAY 1D
 * Fill a one-dimensional array with the multiplication tables
 **********************************************/
void fillArray1D(int grid[], int numRow, int numCol)
{
   for (int row = 0; row < numRow; row++)
      for (int col = 0; col < numCol; col++)
         grid[row * numCol + col] = row * col;
}

/**********************************************************************
 * MAIN
 * Simple driver program to demonstrate fillArray1D and fillArray2D
 ***********************************************************************/
int main()
{
   bool different;
   
   // fill the arrays
   int grid1D[16];                   // sizeof(int) * 16    == 64 bytes
   int grid2D[4][4];                 // sizeof(int) * 4 * 4 == 64 bytes
   fillArray1D(grid1D, 4 /*numCol*/, 4 /*numRow*/);
   fillArray2D(grid2D, 4 /*numRow*/ );

   // verify the output
   different = false;
   for (int row = 0; row < 4; row++)
      for (int col = 0; col < 4; col++)
         if (grid1D[row * 4 + col] != grid2D[row][col])
         {
            cout << "The data is different at location ("
                 << col << ", " << row << ")\n";
            different = true;
         }
   if (!different)
      cout << "The data is the same in both arrays using the [] notation\n";

   // verify the memory representation.  Now this will be a bit tricky. We
   // will create a pointer to an integer which we will direct to the start
   // of grid2D.  Since the data-types are different, we wil need to cast
   // to do this.  From here, we will walk through both arrays using pointer
   // arithmetic.  If we make it all the way through both, then the memory
   // representation is the same
   int * p1D = (int *)grid1D;
   int * p2D = (int *)grid2D;
   different = false;
   for (int offset = 0; offset < (4 * 4); offset++)
   {
      if (*p1D != *p2D)
      {
         cout << "The memory representation is different at offset "
              << offset
              << endl;
         different = true;
      }
      p1D++;
      p2D++;
   }
   if (!different)
      cout << "The memory representation is the same for both arrays\n";

   return 0;
}

