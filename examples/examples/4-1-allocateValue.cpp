/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to allocate memory and use it
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/**********************************************************************
 * MAIN
 * A contrived example, that is all
 ***********************************************************************/
int main()
{
   // At first, the pointer refers to nothing.  We use the NULL pointer
   // to signify the address is invalid and uninitialized
   float * pNumber = NULL;

   // now we allocate 4 bytes for a float
   pNumber = new(nothrow) float;
   if (!pNumber)                    // always check to see if we succeeded
      return -1;

   // at this point (no pun intended), we can use it like any other pointer
   assert(pNumber);      // better not be the NULL pointer
   *pNumber = 3.14159;

   // regular variables get recycled after they fall out of scope.  Not
   // true with allocated data.  We need to free it with delete
   delete pNumber;
   pNumber = NULL;    // just to make sure I don't try to use it again
   return 0;
}

