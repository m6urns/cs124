/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use a conditional expression
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * MAIN: Just a simple demo program
 ***********************************************************************/
int main()
{
   int num = 4; // number of rows and columns in our table

   for (int row = 1; row <= num; row++)     // count through the rows
      for (int col = 1; col <= num; col++)  // count through the columns
         cout << (row * col)                // display the product
              << (col == num ? '\n' : '\t');// tab or newline, depending

   return 0;
}

