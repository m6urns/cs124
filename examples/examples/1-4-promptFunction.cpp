/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to write a function to prompt the user for
 *      some data (a number in this case) and return it back to the
 *      calling function.  We call this a "prompt function"
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * GET AGE
 * Prompt the user for his age.  First display a message stating
 * what information we hope the user will provide.  Next receive
 * the user input.  Finally, return the results to the caller.
 **********************************************************************/
int getAge()
{
   int age;                      // we need a variable to store the input
   cout << "What is your age? "; // state what you want the user to give
   cin  >> age;                  // we need a variable to store the input
   return age;                   // this sends the data back to main()
}

/**********************************************************************
 * MAIN
 * The whole purpose of main() is to test our getAge() function
 ***********************************************************************/
int main()
{
   // get the user input
   int age = getAge();        // store the data from getAGe() in a variable

   // display the results
   cout << "Your age is "    // note the space after "is"
        << age               // the value from getAge() is stored here
        << " years old.\n";  // again a sapce before "year"

   return 0;                 // return "success"
}

