/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate an online desk check
 ************************************************************************/

#include <iostream>
using namespace std;

// to turn off the desk check, comment out the following line
#define DEBUG

#ifdef DEBUG
#define Debug(x) x
#else
#define Debug(x)
#endif // !DEBUG

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // Instructions
   cout << "This program will sum all the numbers under\n"
        << "twenty counting by X.  Please specify\n"
        << "the X: ";

   // Fetch and validate the data
   int inc;
   cin  >> inc;
   if (inc < 1 || inc > 20)
   {
      cout << "ERROR: Value must be greater than 0 and less than 20\n";
      return 1;
   }

   // Get read the loop
   int sum = 0;
   Debug(cout << "#\tinc\ti\tsum\n");
   Debug(cout << "1\t" << inc << '\t' << '\\' << '\t' << sum << endl);

   // Count the values
   for (int i = inc; i < 20; i+= inc)
   {
      Debug(cout << "2\t" << inc << '\t' << i << '\t' << sum << endl);
      sum += i;
      Debug(cout << "3\t" << inc << '\t' << i << '\t' << sum << endl);
   }
   Debug(cout << "4\t" << inc << '\t' << '\\' << '\t' << sum << endl);

   // Display the results and quit!
   cout << "The sum is: " << sum << endl;
   return 0;
}
