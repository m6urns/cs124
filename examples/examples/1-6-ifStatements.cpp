/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to use both kinds of IF statements: the
 *      Action-A/Action-B type and the Action/No-Action type.
 ************************************************************************/

#include <iostream>
using namespace std;

/******************************************************
 * VALID GPA
 *     Demonstrate an Action-A/Action-B IF statement
 *****************************************************/
bool validGpa(float gpa)
{
   if (gpa > 4.0 || gpa < 0.0)   // Boolean expression
      return false;              // True condition or Action-A
   else
      return true;               // False condition or Action-B
}

/**********************************************************************
 * main(): Prompt the user for his GPA and display whether the value
 *         was valid.
 ***********************************************************************/
int main()
{
   float gpa;
   
   // prompt for GPA
   cout << "Please enter your GPA: ";
   cin  >> gpa;
   
   // give error message if invalid.
   if (!validGpa(gpa))                                // Boolean expression
      cout << "Your GPA is not in the valid range\n"; // Action/No-Action

   return 0;
}

