/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to walk through a string backwards
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/****************************************************************
 * DISPLAY BACKWARDS
 * Display a string backwards
 ****************************************************************/
void displayBackwards(const char text[])
{
   // first find the end of the string
   int i = 0;                    // need to be a local because both loops
                                 //    need to use the same index
   while (text[i])               // as long as the null is not found
      i++;                       // keep going through the string
   assert(text[i] == '\0');      // this better be the null character
   
   // now go backwards
   for (i--; i >= 0; i--)        // back up one space; we went too far
      cout << text[i];           // display the text
}

/**********************************************************************
 * MAIN
 * Just a simple driver program for displayBackwards()
 ***********************************************************************/
int main()
{
   // prompt for the text
   char text[256];
   cout << "Please enter some text: ";
   cin.getline(text, 256);
   
   // display it backwards
   cout << "The text backards is: \"";
   displayBackwards(text);
   cout << "\"\n";

   return 0;
}

