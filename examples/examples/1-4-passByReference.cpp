/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate pass-by-value, and pass-by-reference
 ************************************************************************/

#include <iostream>
using namespace std;

/*************************************************
 * PASS NOTHING
 * No information is being sent to the function
 ************************************************/
void passNothing()
{
   cout << "passNothing()\n";
   
   // a different variable than the one in MAIN
   int value;
   value = 0;
}

/**************************************************
 * PASS BY VALUE
 * One-way flow of information from MAIN to the function.  No
 * data is being sent back to MAIN
 *************************************************/
void passByValue(int value)
{
   // show the user what value was sent to the function
   cout << "passByValue(" << value << ")\n";

   // this is a copy of the variable in MAIN.  This will not
   // influence MAIN in any way:
   value = 1;
}

/************************************************
 * PASS BY REFERENCE
 * Two-way flow of data between the functions.  In this
 * case, changes to REFERENCE will also influence the variable
 * in MAIN even though they have different names
 ***********************************************/
void passByReference(int &reference)
{
   // show the user what value was sent to the function
   cout << "passByReference(" << reference << ")\n";

   // this will actually change MAIN because there was the &
   // in the parameter
   reference = 2;
}

/***************************************************
 * MAIN
 * This is just a silly program to demonstrate passing
 ***************************************************/
int main()
{
   int value = 10;
   
   // first, pass nothing.  There will be no influence on the variables
   // in main.
   cout << "================= passNothing =================\n";
   cout << "Before: " << value << endl;
   passNothing();
   cout << "After:  " << value << endl;

   // next, pass-by-value.  We will not be changing MAIN's copy here
   cout << "================= passByValue =================\n";
   cout << "Before: " << value << endl;
   passByValue(value);
   cout << "After:  " << value << endl;
   
   // finally, pass-by-reference.  MAIN will change here
   cout << "================= passByReference =============\n";
   cout << "Before: " << value << endl;
   passByReference(value);
   cout << "After:  " << value << endl;

   return 0;
}
