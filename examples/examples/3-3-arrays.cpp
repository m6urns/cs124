/***********************************************************************
 * This demo program is designed to:
 *      Passing arrays as a parameter using the pointer notation
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

/***************************
 * INITIALIZE ARRAY
 * Set the values of an array
 * to zero
 ****************************/
void initializeArray(int * array,      // we could say int array[] instead, it
                     int size)         //     means basically the same thing
{
   for (int i = 0; i < size; i++)      // standard array loop
      array[i] = 0;                    // use the [] notation even though we
}                                      //     declared it as a pointer

/***************************
 * MAIN
 * Simple driver program
 ***************************/
int main()
{
   const int SIZE = 10;                 // const variables are ALL_CAPS
   int list[SIZE];                      // can be declared as a CONST
   assert(SIZE == sizeof(list) / sizeof(*list));
   assert(SIZE == sizeof(list) / sizeof(list[0]));

   // call it the normal way
   initializeArray(list, SIZE);         // call the function the normal way

   // call it with a pointer
   int * pList = list;                  // list is a pointer so this is OK
   initializeArray(pList, SIZE);        // exactly the same as the first time
                                        //       we called initializeArray
   return 0;
}
