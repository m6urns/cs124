/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate linear searching
 ************************************************************************/

#include <iostream>
using namespace std;

bool linearSearch(const int numbers[], int size, int search);

/**********************************************************************
 * MAIN
 * Driver program for linearSearch()
 ***********************************************************************/
int main()
{
   // list to search in
   int values[] =
   {
      1, 3, 5, 7, 7, 8, 9, 11, 13
   };

   // the value to search for
   int search;
   cout << "Which item do you want to find? ";
   cin  >> search;
   
   // execute the linear search function and display the results
   if (linearSearch(values, sizeof(values) / sizeof(values[0]), search))
      cout << "\tLinear found it!\n";
   else
      cout << "\tLinear did not find it.\n";

   return 0;
}

/**************************************************************
 * LINEAR SEARCH
 *    perform a linear search
 * INPUT:
 *    numbers:   a list of numbers to search through
 *    size:      the size of the numbers list
 *    search:    the number that we are searching for
 * OUTPUT:
 *    bool:      true if found, false if not found
 **************************************************************/
bool linearSearch(const int numbers[], int size, int search)
{
   // walk through every element on the list
   for (int i = 0; i < size; i++)   // standard FOR loop for an array
      if (search == numbers[i])     // compare against the search term
         return true;               // if found, leave with true

   // not found if we reached the end
   return false;
}
