/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to pass arrays as parameters between functions.
 ************************************************************************/

#include <iostream>
using namespace std;

#define NUM_PRICES 5
#define CONVERSION_RATE 1.3778

/****************************************
 * GET PRICES
 * Prompt the user for the stock prices
 * in Euros
 ***************************************/
void getPrices(float prices[], int num)
{
   cout << "Please enter " << num << " prices in Euros\n";

   for (int i = 0; i < num; i++)
   {
      cout << "\tPrice # " << i + 1 << ": ";
      cin  >> prices[i];
   }
}

/************************************
 * CONVERT
 * Convert the prices from Euros to
 * US Dollars
 ***********************************/
void convert(float prices[], int num)
{
   for (int i = 0; i < num; i++)
      prices[i] *= CONVERSION_RATE;
}

/****************************************
 * DISPLAY
 * Display the prices in US dollars
 **************************************/
void display(const float prices[], int num)
{
   // configure the output for money
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);

   // display the prices
   cout << "The prices in US dollars are:\n";
   for (int i = 0; i < num; i++)
      cout << "\t$" << prices[i] << endl;
}

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // fill the prices array
   float prices[NUM_PRICES];
   getPrices(prices, NUM_PRICES);
   
   // convert to dollars from euros
   convert(prices, NUM_PRICES);

   // display the results
   display(prices, NUM_PRICES);

   return 0;
}

