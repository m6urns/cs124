/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate growing or re-allocating arrays. We will be reading
 *      all the data from a file into an array that will be grow to
 *      fit.
 ************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

char *readData(const char *fileName);

/**********************************************************************
 * MAIN: Simple driver program for readFile and reallocate
 ***********************************************************************/
int main()
{
   // get the fileName
   char fileName[256];
   cout << "Filename: ";
   cin  >> fileName;

   // read the data
   char * pData = readData(fileName);
   
   // clean up when finished
   delete [] pData;

   return 0;
}

/*********************************************
 * REALLOCATE
 * Create a new buffer twice the size of bufferOld,
 * copy the data from bufferOld to bufferNew,
 * free bufferOld, and return bufferNew
 *********************************************/
char * reallocate(char * bufferOld, int &size)
{
   cout << "reallocating from " << size << " to " << size * 2 << endl;
   
   // allocate the new buffer
   char * bufferNew = new(nothrow) char[size *= 2];
   if (NULL == bufferNew)
   {
      cout << "Unable to allocate a buffer of size " << size << endl;
      size /= 2;              // reset the size
      return bufferOld;
   }
   
   // copy the data into the new buffer
   int i;
   for (i = 0; bufferOld[i]; i++)   // use index because it is easier than
      bufferNew[i] = bufferOld[i];  //   two pointers
   bufferNew[i] = '\0';             // don't forget the NULL
   
   // delete the old buffer
   delete [] bufferOld;

   // return the new buffer
   return bufferNew;
}

/*************************************
 * READ DATA
 * Read all the data from a file and put
 * it in a single character string.
 ************************************/
char * readData(const char * fileName)
{
   // create the initial buffer
   char * pBuffer = new char[4]; // buffer we will be filling
   int max = 4;                  // max size of the buffer right now
   int size = 0;                 // how much of the buffer is filled now
   pBuffer[0] = '\0';            // initialize the buffer to empty string
   
   // open the file
   ifstream fin(fileName);
   if (fin.fail())
      return pBuffer;   // just return the empty string

   // read the data from the file
   while (!fin.eof())
   {
      // grow the buffer as needed
      if (size + 1 >= max)
         pBuffer = reallocate(pBuffer, max);

      // read the data
      pBuffer[size] = fin.get();  // get the next character
      pBuffer[++size] = '\0';     // keep the string well-formed      
   }
   
   // display the size and return the buffer
   cout << "Total size: " << size << endl;
   fin.close();
   return pBuffer;
   
}

