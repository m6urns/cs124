/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to create, populate, and free a jagged array
 ************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // Prompt the user for the number of letters
   cout << "How many letters are there in your "
        << "first, middle, and last name?\n";
   int sizeFirst;
   int sizeMiddle;
   int sizeLast;
   cin >> sizeFirst >> sizeMiddle >> sizeLast;
   cin.ignore();  // skip that extra newline before the getlines
   
   // allocate the jagged array
   char ** names;
   names = new char *[3];  // three names
   names[0] = new char[sizeFirst  + 1];
   names[1] = new char[sizeMiddle + 1];
   names[2] = new char[sizeLast   + 1];

   // fill the jagged array
   cout << "Please enter your first name: ";
   cin.getline(names[0], sizeFirst + 1);
   cout << "Please enter you middle name: ";
   cin.getline(names[1], sizeMiddle + 1);
   cout << "Please enter your last name: ";
   cin.getline(names[2], sizeLast + 1);

   // display the results
   cout << "Your name is \""
        << names[0] << ' '
        << names[1] << ' '
        << names[2] << "\"\n";

   // free the array
   delete [] names[0];
   delete [] names[1];
   delete [] names[2];
   delete [] names;

   return 0;
}

