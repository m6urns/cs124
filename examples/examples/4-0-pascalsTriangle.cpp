/***********************************************************************
 * This demo program is designed to:
 *      Create Pascal's triangle
 *           1  1  1  1  1
 *           1  2  3  4  5
 *           1  3  6  10 15
 *           1  4  10 20 35
 *           1  5  15 35 70
 *      Please see en.wikipedia.org/wiki/Pascal's_triangle
 ************************************************************************/

#include <iostream>    // for COUT
#include <iomanip>     // for SETW
using namespace std;

#define SIZE    11
#define SPACING 7

void fill(         int grid[][SIZE]);
void display(const int grid[][SIZE]);

/**********************************************************************
 * MAIN
 * A driver program to create and display Pascal's triangle (turned on
 * its side).
 ***********************************************************************/
int main()
{
   // declare the array
   int grid[SIZE][SIZE];

   // fill the grid
   fill(grid);

   // display the results
   display(grid);

   return 0;
}

/********************************************************
 * FILL
 * Fill the grid using the following algorithm:
 *  1. On the first row, the values are: 1 1 1 ...
 *  2. The first item on a new row is also the value 1
 *  3. Every other item is the sum of the previous
 *     row and the previous column
 ********************************************************/
void fill(int grid[][SIZE])
{
   // 1. fill the first row
   for (int column = 0; column < SIZE; column++)
      grid[0][column] = 1;

   for (int row = 1; row < SIZE; row++)
   {
      // 2. the first item on a new row is 1
      grid[row][0] = 1;
      
      // 3. every other item is the sum of the item above and to the left
      for (int column = 1; column < SIZE; column++)
         grid[row][column] = grid[row - 1][column] + grid[row][column - 1];
   }
}

/****************************************************
 * DISPLAY
 * Display the grid on the screen
 ****************************************************/
void display(const int grid[][SIZE])
{
   for (int row = 0; row < SIZE; row++)
   {
      for (int column = 0; column < SIZE; column++)
         cout << setw(SPACING) << grid[row][column];
      cout << endl;
   }
}

