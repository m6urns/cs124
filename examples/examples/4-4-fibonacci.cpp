/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate two methods for computing a Fibonacci sequence.
 *      These two methods will then be compared for efficiency by
 *      instrumenting the code.
 ************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

int computeFibonacciArray(int n);
int computeFibonacciRecursive(int n);

#ifdef DEBUG
// all the code that will only execute if DEBUG is defined
int countIterations = 0;
#define increment()     countIterations++
#define reset()         countIterations = 0
#define getIterations() countIterations

#else // !DEBUG
#define increment()
#define reset()
#define getIterations()  0

#endif

/**********************************************************************
 * main()...
 ***********************************************************************/
int main()
{
   // prompt for the number of items in the series
   int num;
   cout << "How many Fibonacci numbers shall we identify? ";
   cin  >> num;

   // compute the values
   reset();
   int valueArray = computeFibonacciArray(num);
   int costArray  = getIterations();
   reset();
   int valueRecursive = computeFibonacciRecursive(num);
   int costRecursive  = getIterations();
   
   // display the values
#ifdef DEBUG
   cout << "Number of iterations for the array method: "
        << costArray << endl;
   cout << "Number of iterations for the recursive method: "
        << costRecursive << endl;
#else // !DEBUG
   cout << "Array method:     " << valueArray     << endl;
   cout << "Recursive method: " << valueRecursive << endl;
#endif // !DEBUG
   
   return 0;
}

/****************************************
 * COMPUTE FIBONACCI ARRAY
 * Compute the Nth Fibonacci numbers
 * using the array method
 ***************************************/
int computeFibonacciArray(int n)
{
   // allocate an array
   assert(n > 0);
   int *array = new(nothrow) int[n + 1];
   if (NULL == array)
      return 0;

   // the first two are special
   array[0] = 0;
   array[1] = 1;

   for (int i = 2; i <= n; i++)
   {
      increment();
      array[i] = array[i - 1] + array[i - 2];
   }
   
   // clean up when done
   int lastValue = array[n];
   delete [] array;
   return lastValue;
}

/***************************************
 * COMPUTE FIBONACCI RECURSIVE
 * Compute the Nth Fibonacci numbers
 * using a recursive algorithm. 
 **************************************/
int computeFibonacciRecursive(int n)
{
   assert(n >= 0);
   
   switch (n)
   {
      case 0:
         increment();
         return 0;
      case 1:
         increment();
         return 1;
      default:
         increment();
         return computeFibonacciRecursive(n - 1) +
            computeFibonacciRecursive(n - 2);
   }
}
