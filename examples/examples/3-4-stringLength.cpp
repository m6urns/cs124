/***********************************************************************
 * This demo program is designed to:
 *      demonstrate three ways to compute the length of a string:
 *      1. using array index notation (stringLengthArray())
 *      2. using standard pointer FOR loop (stringLengthPointer())
 *      3. optimal solution (stringLengthOptimal())
 ************************************************************************/

#include <iostream>
using namespace std;

int stringLengthArray(  const char * text);
int stringLengthPointer(const char * text);
int stringLengthOptimal(const char * text);

/**********************************************************************
 * MAIN
 * Simple driver program for our three string functions
 ***********************************************************************/
int main()
{
   // prompt
   char text[256];
   cout << "Please enter your text: ";
   cin.getline(text, 256);
   
   // display the results
   cout << "\tArray notation:    "
        << stringLengthArray(text)
        << endl;
   cout << "\tPointer notation:  "
        << stringLengthPointer(text)
        << endl;
   cout << "\tOptimal version:   "
        << stringLengthOptimal(text)
        << endl;

   return 0;
}

/*************************************************
 * STRING LENGTH: array index version
 * The index of the null character is the length
 * of the string
 ************************************************/
int stringLengthArray(const char * text)
{
   int index;                           // declare out of the FOR loop scope

   for (index = 0; text[index]; index++)// loop unitl index of null is found
      ;                                 // no body: just looking for null
   
   return index;                        // the index of the null character
}

/***************************************************
 * STRING LENGTH: pointer version
 * Increment the length variable with every iteration
 ***************************************************/
int stringLengthPointer(const char * text)
{
   int length = 0;                      // length initially zero

   for (const char * p = text; *p; p++) // second standard FOR loop
      length++;                         // increment length with every
                                        //    non-null character
   
   return length;                       // return the length
}

/**************************************************
 * STRING LENGTH: optimal version
 * Measure how far the pointer traveled
 **************************************************/
int stringLengthOptimal(const char * text)
{
   const char * p = text;   // point at the start of the string

   while (*(p++))           // advance the pointer while doing a null check.
      ;                     //    In the end, p will point at the null
   
   return p - text - 1;     // the difference between the head and the tail
}
