/***********************************************************************
* Program:
*    Assignment 10, First program
*    Brother Helfrich, CS124
* Author:
*    Br. Helfrich
* Summary: 
*    This program will put the text "CS 124" on the screen.  It will
*    not win any Nobel Prizes nor make the author rich.
*
*    Estimated:  0.2 hrs   
*    Actual:     0.1 hrs
*      Emacs is different than anything else I have ever used!
************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
* In a few days, we will have more than one function in our programs.  As
* for now, main() will have to do...
***********************************************************************/
int main()
{
   // Display the text
   cout << "CS 124\n";

   // Return when done
   return 0;
}
