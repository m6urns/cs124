/****************************************************************
 * This program is designed to determine the speed in which 
 * the string class append performs its operation.  In the process
 * we hope to gain some insight into how the string class works
 ****************************************************************/

#include <iostream>
#include <iomanip>
#include <string.h>
using namespace std;

#include <time.h>

/*******************************************
 * Append the letter 'a' on to the string a total
 * of 'number' times.  Return how long the process
 * took in ms.  
 * This uses the slow strlen method of finding the end
 ********************************************/
int simpleCString(int number)
{
   int msBegin = clock();

   // prepare to start the experiment
   char * cstring = new char[number + 1];   // allocate the string
   cstring[0] = '\0';                       // mark it as empty

   // perform the experiement
   while (number--)
   {
      int length = strlen(cstring);         // find the end
      cstring[length]     = 'a';            // append the 'a'
      cstring[length + 1] = '\0';           // mark it as NULL
   }

   // clean up after ourselves
   delete [] cstring;

   int msEnd = clock();
   return msEnd - msBegin;
}

/*******************************************
 * Append the letter 'a' on to the string like above.
 * This remembers the length of the string so it is faster.
 ********************************************/
int smartCString(int number)
{
   int msBegin = clock();

   // prepare to start the experiment
   char * cstring = new char[number + 1]; // allocate the string
   int length = 0;                        // set length to zero
   cstring[0] = '\0';                     // set the string to empty

   // perform the appends
   while (number--)
   {
      cstring[length]   = 'a';            // append on the letter
      cstring[++length] = '\0';           // ensure we are NULL terminated
   }

   // clean up after ourselves
   delete [] cstring;

   int msEnd = clock();
   return msEnd - msBegin;
}


/*******************************************
 * Append the letter 'a' on to the string like above.
 * This uses the string class
 ********************************************/
int stringClass(int number)
{
   int msBegin = clock();
   string str;
   str.resize(number);
   
   for (int i = 0; i < number; i++)
      str += "a";

   int msEnd = clock();

   return msEnd - msBegin;
}

/********************************************
 * Test the speed in which various string functions work.
 ********************************************/
int main(int argc, char **argv)
{

   // conduct the tests
   cout << setw(15) << "number"
        << setw(15) << "strlen"
        << setw(15) << "smart strlen"
        << setw(15) << "string class" 
        << endl;
   for (int i = 1000; i < 100000000; i *= 2)
      cout << setw(15) << i
           << setw(15) << simpleCString(i) / 1000
           << setw(15) << smartCString(i) / 1000
           << setw(15) << stringClass(i) / 1000
           << endl;

   return 0;
}
