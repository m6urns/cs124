/***********************************************************************
 * This demo program is designed to:
 *      Demonstrate how to read a list of numbers from a file.  Note that
 *      the size of the file is unknown.
 ************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;


#define MAX 100

int readFile(const char fileName[], int data[], int max);
void display(const int data[], int num);

/**********************************************************************
 * MAIN
 * Read data from a file and display the data
 ***********************************************************************/
int main()
{
   // fetch the filename
   char fileName[256];
   cout << "Please enter filename: ";
   cin  >> fileName;

   // read the data from the file
   int data[MAX];
   int numRead = readFile(fileName, data, MAX);
   
   // display the results
   display(data, numRead);

   return 0;
}

/********************************************
 * READ FILE
 * Read the data from the file.  Make sure
 * there is no more data in the file than
 * we have room in our array!
 *******************************************/
int readFile(const char fileName[],    // use const because it will not change
             int data[],               // the output of the function
             int max)                  // capacity of data, it will not change
{
   // open the file for reading
   ifstream fin(fileName);             // open the input stream to fileName
   if (fin.fail())                     // never forget the error checking
   {
      cout << "ERROR: Unable to read file "
           << fileName                 // display the filename we tried to read
           << endl;
      return 0;                        // return the error condition: none read
   }

   // loop through the file, reading the elements one at a time
   int numRead = 0;                    // initially none were read
   while (numRead < max &&             // don't read more than the list holds
          fin >> data[numRead])        // read and check for errors
      numRead++;                       // increment the index by one
   
   // close the file and return
   fin.close();                        // never forget to close the file
   return numRead;                     // report the number successfully read
}

/************************************
 * DISPLAY
 * Display all the data read from the file
 ************************************/
void display(const int data[], int num)
{
   // standard FOR loop...
   for (int i = 0; i < num; i++)
      cout << "\t" << data[i] << endl;
}
