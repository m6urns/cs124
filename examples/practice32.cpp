/***********************************************************************
* Program:
*    Practice 3.2, Threshold
*    Brother Helfrich, CS 124
* Author:
*    James Helfrich
* Summary: 
*    This program is designed to valided user input against a specified
*    set of working parameters.  
************************************************************************/

#include <iostream>
#include <fstream>   // Don't forget this guy!
#include <iomanip>
using namespace std;

#define MAX 100

/****************************************************************
 * READ
 * Read the array from a file and dump it into an array.  Return
 * false if anything bad happens
 ***************************************************************/
bool read(char fileName[], float mileage[], int &numItems)
{
   // open the file
   ifstream fin(fileName);
   if (fin.fail())
      return false;

   // read the array as long as there is still room and
   // we have not reached the end of the file
   numItems = 0;
   while (fin >> mileage[numItems] && numItems < MAX)
      numItems++;

   // bail if we failed to read anything from the file.  This could
   // mean an empty file or it could mean that the file was filled with
   // something other than numbers
   if (numItems == 0)
   {
      fin.close();
      return false;
   }
   
#ifdef NEVER
   // another way of doing the same loop as above...
   numItems = 1;                        // number of items read 
   fin >> mileage[0];                   // read in the first item
   while (!fin.eof() && numItems < MAX) // as long as not at the end...
   {
      float tmp;                        // tmp value.  It might be bogus!
      fin >> tmp;                       // read it in.
      if (!fin.fail())                  // if the file is empty, we will know
         mileage[numItems++] = tmp;     // now the value is known to be good
   }
#endif // NEVER
   
   // make like a tree
   fin.close();
   return true;
}

/************************************************************
 * DISPLAY THRESHOLD
 * Write to the screen only those items in mileage[] that are
 * below the threshold value
 ***********************************************************/
void displayThreshold(float mileage[], int numItems, float threshold)
{
   // format output
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(1);

   // loop through the list and only display those items that are
   // below the threshold
   cout << "The following items are below the threshold:\n";
   for (int i = 0; i < numItems; i++)
      if (mileage[i] < threshold)
         cout << '\t' << mileage[i] << endl;

   return;
}

/***********************************************************
 * GET FILENAME
 * Prompt the user for the filename
 **********************************************************/
void getFilename(char fileName[])
{
   cout << "What is the name of the file: ";
   cin  >> fileName;
   return;
}

/*******************************************************
 * GET THRESHOLD
 * Prompt the user for the threshold value
 *******************************************************/
float getThreshold()
{
   float threshold;
   cout << "What is the threshold: ";
   cin  >> threshold;

   return threshold;
}

/**********************************************************************
* Main does not do much; most of the work is done by the functions
***********************************************************************/
int main()
{
   char  fileName[256];   // name of the file to be read
   float mileage[MAX];    // array where the data will be stored
   int   numItems;        // number of valid items within the array
   
   // read the file
   getFilename(fileName);
   if (!read(fileName, mileage, numItems))
   {
      cout << "Error reading file " << fileName << endl;
      return 1;
   }
   
   // display the threshold
   float threshold = getThreshold();
   displayThreshold(mileage, numItems, threshold);
   
   // make like a tree
   return 0;
}
