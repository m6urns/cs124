/***********************************************************************
* Program:
*    Practice 1.1, GPA
*    Brother Helfrich, CS124
* Author:
*    The Key
* Summary: 
*    This program is designed to ask the user for his GPA and return the
*    results on the screen.
************************************************************************/

#include <iostream>
using namespace std;

/****************************************************************
 * GET GPA
 * Prompt the user for his GPA
 ***************************************************************/
float getGpa()
{
   // PROMPT the user for his GPA
   cout << "What is your GPA? ";

   // GET the GPA
   float gpa;
   cin >> gpa;

   return gpa;
}

/*****************************************************************
 * DISPLAY
 * Display the results on the screen
 *****************************************************************/
void display(float gpa)
{
   // set the floating point display options
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(1);

   // PUT the answer
   cout << "Your GPA is " << gpa << endl;

   return;
}

/**********************************************************************
* Just ask the user for his GPA
***********************************************************************/
int main()
{
   float gpa;

   // prompt for GPA
   gpa = getGpa();

   // display the results
   display(gpa);

   return 0;
}
