/***********************************************************************
* Program:
*    Practice 3.1, Grades
*    Brother Helfrich, CS124
* Author:
*    The Key
* Summary: 
*    A simple program to keep track of some grades
************************************************************************/

#include <iostream>
#include <cassert>
using namespace std;

const int MAX = 100;

int numberGrades();
void getGradeList(int grades[], int num);
void validateGradeList(int grades[], int num);
int average(int grades[], int num);

/**********************************************************************
* Get the list of grades from the user, validate them, and display the
* average
***********************************************************************/
int main()
{
   int grades[MAX];      // list of grades
   int num;              // number of grades in the list

   // fill the list
   getGradeList(grades, num = numberGrades());

   // validate list;
   validateGradeList(grades, num);

   // display the average
   cout << "The average grade is "
        << average(grades, num)
        << "%\n";
   
   // we are outta here.
   return 0;
}


/*****************************************************
 * NUMBER GRADES
 * Prompt the user for the number of grades
 ****************************************************/
int numberGrades()
{
   int num;
   cout << "How many grades? ";
   cin  >> num;

   // paranoid much?
   assert(num < MAX && num > 0);
   // it would be better to handle the user error rather than
   // assert here.
   return num;
}

/**********************************************
 * GET GRADE LIST
 * Prompt the user for the grades to fill the array
 *********************************************/
void getGradeList(int grades[], int num)
{
   // yes, yes, we have established that I am paraniod
   assert(num < MAX && num > 0);

   // loop through the values
   for (int i = 0; i < num; i++)
   {
      cout << "\tEnter grade "
           << i + 1
           << ": ";
      cin  >> grades[i];
   }

   return;
}

/*************************************************
 * VALIDATE GRADE LIST
 * Look for invalid grades and re-prompt the user
 ************************************************/
void validateGradeList(int grades[], int num)
{
   // enough already!
   assert(num < MAX && num > 0);

   // look at all the values in the array
   for (int i = 0; i < num; i++)
      if (grades[i] > 100 || grades[i] < 0)
      {
         cout << "\tGrade "
              << i + 1
              << " is not valid. Please re-enter grade "
              << i + 1
              << ": ";
         cin  >> grades[i];

      }

   return;
}

/**********************************************
 * AVERAGE
 * find the average grade.
 *********************************************/
int average(int grades[], int num)
{
   assert(num > 0 && num < MAX);

   // find the sum
   int sum = 0;
   for (int i = 0; i < num; i++)
      sum += grades[i];

   // return the average
   return sum / num;
}
