/***********************************************************************
* Program:
*    Test 2, Powers of two
*    Brother Helfrich, CS124
* Author:
*    The Key
* Summary: 
*    Display the powers of two
*
************************************************************************/

#include <iostream>
using namespace std;

int promptForNumber();
void displayPowers(int number);

/**********************************************************************
 * MAIN
 * Prompt for the powers of two and display the results
 ***********************************************************************/
int main()
{
   // how many powers will we display?
   int number = promptForNumber();

   // display the results
   displayPowers(number);
   
   return 0;
}

/***********************************************************
 * PROMPT FOR NUMBER
 * Promp the user for how many powers we will cycles through
 **********************************************************/
int promptForNumber()
{
   int number;
   cout << "How many powers of two: ";
   cin  >> number;
   return number;
}

/********************************************************
 * DISPLAY POWERS
 * Display the first number powers of two
 *******************************************************/
void displayPowers(int number)
{
   // it is far easier to keep a running total rather than so some
   // fancy powers mathematics
   int power = 1;    // 2^0 == 1
   
   // loop through the values
   for (int exponent = 0; exponent < number; exponent++)
   {
      // display the power
      cout << "\t2^"
           << exponent
           << "\t"
           << power
           << endl;

      // compute the next one
      power *= 2; 
   }
}

