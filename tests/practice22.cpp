/***********************************************************************
* Program:
*    Test 2, ASCII Values
*    Brother Helfrich, CS124
* Author:
*    The Key
* Summary: 
*    This program implement a guessing game where the user tries to
*    guess the ASCII Value for a given character.
*
************************************************************************/

#include <iostream>
using namespace std;

char promptForLetter();
bool guessValue(char guess, char answer);
int  playGame(char answer);

/**********************************************************************
 * MAIN
 * Prompt the user for the value, loop through the guesses, and display
 * the number of tries.
 ***********************************************************************/
int main()
{
   // prompt for letter
   char answer = promptForLetter();

   // play the game
   int numberGuesses = playGame(answer);

   // display the results
   cout << "You got it in "
        << numberGuesses
        << " guesses.\n";
   
   return 0;
}

/****************************************************
 * PROMPT FOR LETTER
 * Prompt for a letter for the user to guess
 ****************************************************/
char promptForLetter()
{
   char guess;
   cout << "Which value would you like to guess: ";
   cin  >> guess;
   return guess;
}

/**************************************************
 * GUESS VALUE
 * Determine if the guess is the value.  Return TRUE
 * if they match, display a message if they don't
 *************************************************/
bool guessValue(int guess, char answer)
{
   // do they match
   if (guess == answer)
      return true;

   // guess is too small
   if (guess > answer)
      cout << "Too high\n";
   else
      cout << "Too low\n";
   return false;
}

/***************************************************
 * PLAY GAME
 * Loop through the guesses, returning the number of
 * tries it took to get the answer
 *************************************************/
int playGame(char answer)
{
   // how many guesses, initialized to zero
   int numGuesses = 0;

   // this needs to be an integer so we can compare this number
   // against the character in 'answer'
   int guess;

   // play the game
   do
   {
      cout << "What is the value of \'"
           << answer
           << "\'? ";
      cin  >> guess;                       // prompt for user's guess
      numGuesses++;                        // tally up the guesses
   }
   while (!guessValue(guess, answer));

   // return and report
   return numGuesses;
}

