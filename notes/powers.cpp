#include <iostream>

using namespace std;

int getNumberOfPowers();
void displayPowers(int numberOfPowers);

int main()
{
   //Value of getNumberPower to number
   int number = getNumberOfPowers();
   //Pass number, same thing
   displayPowers(number);
   
}

int getNumberOfPowers()
{
   int powers;
   cout << "How many powers of two: ";
   cin >> powers;

   return powers;
}

void displayPowers(int numberOfPowers)
{
   double answer = 1;

   for(int i = 0; i < numberOfPowers; i++)
      {
         cout << "\t2^" << i << "\t" << (answer) << endl;

         answer *= 2;
      }
}
