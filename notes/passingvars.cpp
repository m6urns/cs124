#include <iostream>

using namespace std;

float divideTwoNumbers (float x, float y)
{
float answer = x / y;
return answer;
}
int main()
{
//Variable x, y are passed into funtion divide and the return is stored to
//z. Prints the value of z. Floating a var and passing in info passes
//info to function and brings it back to variable, automatically.
float z = divideTwoNumbers(12, 3);

cout << "the answer is:" << z << endl;
}

