#include <iostream>

using namespace std;

float getTotalGradePoints();
float calculateGPA(float totalGradePoints);
void displayGPA(float GPA);

int main()
{
   float GPA = getTotalGradePoint();

   displayGPA(GPA);
   
   return 0;
}

float getTotalGradePoint();
{
   char grade;
   float counter = 0.0; 
   float total = 0.0;

   do
   { 
      cout << "Enter a grade: ";
      cin >> grade:
      
      if (grade == 'A')
         total = total + 4.0;  
      else if (grade == 'B')
         total = total + 3.0;
      else if (grade == 'C')
         total = total + 2.0;
      else if (grade == 'D')
         total = total + 1.0;
      else (grade == 'F')
         total = total + 0.0
      counter++; 
   }    
   while (grade != 'Q');
 
   float GPA = (total / counter);

   return GPA;  
}

void displayGPA(float GPA)
{
   cout << "Your GPA: " << GPA;
   
   if (GPA < 1.0)
      cout << " [F]" << endl;
   else if (GPA < 2.0 && GPA > 1.0)
      cout << " [D]" << endl;
   else if (GPA < 3.0 && GPA > 2.0)
      cout << " [C]" << endl;
   else if (GPA < 4.0 && GPA > 3.0)
      cout << " [B]" << endl;
   else if (GPA >= 4.0)
      cout << " [A]" << endl;
   else 
      cout << "CHEATER" << endl;
}
