/***********************************************************************
* Program:
*    Assignment 13, Temperature Conversion  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This Temperature converter takes a F value and converts the 
*    value into degrees C.
*
*    Estimated:  0.5 hrs   
*    Actual:     0.75 hrs
*      Correctly formatting the final output took some time to figure
*      out, I used setw() at first but eventually noticed that 
*      testbed wanted output aligned left. I also had to remove the decimal 
*      which required that I remove the option cout.setf(ios::showpoint)
************************************************************************/

#include <iomanip>
#include <iostream>
using namespace std;

float tempF;
float tempC;

/**********************************************************************
 * Main asks the user to input a value in F, it converts that value
 * into C and outputs it for the user.
 ***********************************************************************/
int main()
{
   cout.setf(ios::fixed);
   cout.precision(0);
   
   //Ask for Fahrenheit and take input
   cout << "Please enter Fahrenheit degrees: "; 
   cin >> tempF;

   //Work on input C=5/9(F - 32)
   
   tempC = (5.0 / 9.0) * (tempF - 32.0);
   
   //Output Celsius
   cout << "Celsius: " << left << tempC << endl;

   return 0;
}
