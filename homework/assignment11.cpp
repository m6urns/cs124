/***********************************************************************
* Program:
*    Assignment 11, Output          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    The program below prints a budget on the screen, formatted so all
*    the dollar signs line up right. Two libraries need to be imported, 
*    the iomanip for the setw() option and the iostream library.
*
*    Estimated:  1.0 hrs   
*    Actual:     0.75 hrs
*      The thing which took the most time to figure out was the spacing
*      in between the numbers and dollar signs.
************************************************************************/

#include <iomanip>
#include <iostream>
using namespace std;

/**********************************************************************
 * Main prints a formatted budget on the screen. Formatted using the 
 * setw() option and tabs.  
 ***********************************************************************/

int main()
{

   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);

   cout << "\tItem" << setw(20) << "Projected" << endl;
   cout << "\t=============" << setw(12) << "==========" << endl;
   cout << "\tIncome" << setw(10) << "$" << setw(9) << 1000.00 << endl;
   cout << "\tTaxes" << setw(11) << "$" << setw(9) << 100.00 << endl;
   cout << "\tTithing" << setw(9) << "$" << setw(9) << 100.00 << endl;
   cout << "\tLiving" << setw(10) << "$" << setw(9) << 650.00 << endl;
   cout << "\tOther" << setw(11) << "$" << setw(9) << 90.00 << endl;
   cout << "\t=============" << setw(12) << "==========" << endl;
   cout << "\tDelta" << setw(11) << "$" << setw(9) << 60.00 << endl;

   return 0;
}

