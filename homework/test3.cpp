/***********************************************************************
* Program:
*    Test 3, Average Grade          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    Enter a brief description of your program here!
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

void getFile(char fileName[]);
int readFile(char fileName[], char scores[]);
void displayAverage(int average);

/**********************************************************************
 * 
 ***********************************************************************/
int main()
{
   char fileName[256];
   char scores[256];

   float average = readFile(fileName, scores);

   getFile(fileName);
   readFile(fileName, scores);
   displayAverage(average);
  
   return 0;
}

void getFile(char fileName[])
{
   cout << "Please specify the file name: ";
   cin >> fileName;

   cin.ignore();

   return;
}

int readFile(char fileName[], char scores[])
{
   float total = 0;
   int count = 0;
   
   int score;

   ifstream fin(fileName);
   while (fin.fail())
      return 0;

   while (fin >> score)
   {
         scores[count] = score;
   
         count++;
      
   }

   
   for (int i = 0; scores[i]; i++)
   {
      total += scores[i];
   } 

   fin.close();
  
   float average = 0;

   average = (total / count);
 
   return average;  
}

void displayAverage(int average)
{
   cout << "The average grade on the midterm is " << average << "%" << endl;
}

