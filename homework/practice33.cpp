/***********************************************************************
* Program:
*    Assignment ##, ????          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    Enter a brief description of your program here!
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

void getFileName(char fileName[]);
void getLetter(char letter[]);
void readFile(char fileName[], char letter[], char word[]);

/**********************************************************************
 * 
 ***********************************************************************/
int main()
{
   char fileName[256];
   char letter[256];
   
   char word[256];

   getFileName(fileName);
   getLetter(letter);

   readFile(fileName, letter, word);

   return 0;
}

void getFileName(char fileName[])
{
   cout << "What is the name of the file: ";
   cin >> fileName;

   return;
}

void getLetter(char letter[])
{
   cout << "What letter should we count: ";
   cin >> letter;

   return;
}

void readFile(char fileName[], char letter[], char word[])
{
   int count = 0;
   int numLetters = 0;

   ifstream fin(fileName);
   if (fin.fail())
      return;

   while (fin >> word)
   {
      for (int i = 0; word[i]; i++)
      {
         if (word[i] == letter[0])
         {
            numLetters++;
         }
      }
      
      count++;
   }   

   cout << "There are " << numLetters << "'s in this file" << endl;      
}
