/***********************************************************************
* Program:
*    Test 2, Girls Scout Cookies          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program gets input form girl scouts and adds the number of
*    cookies they sell to a running total. It then takes this number and
*    checks to make sure they have all the money they should.
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      I was calculating the inital iteneration of the getCookies 
*      function incorrectly. Throwing off my total value.
************************************************************************/

#include <iostream>
using namespace std;

float getCookies();
float calculateTotal(float totalCookies);
void displayTotal(float totalIncome);

/**********************************************************************
 * The functions main calls variables and facilitates the passing of 
 * information between them. It finally calls the display variable to
 * display the calculated total.
 ***********************************************************************/
int main()
{
   float totalCookies = getCookies();
   float totalIncome = calculateTotal(totalCookies);

   displayTotal(totalIncome);
   
   return 0;
}

/**********************************************************************
 * The functions getCookies gets the total number of boxes of cookies 
 * and returns that value. Sending it to the calculate total function
 * to get a monetary value.
 * *******************************************************************/
float getCookies()
{
   float cookies = 0;
   float totalCookies = 0;

   cout << "Number of boxes sold: ";
   cin >> cookies;
   
   totalCookies += cookies;

   while (cookies != 0)
   {
      cout << "Number of boxes sold: ";
      cin >> cookies;
   
      totalCookies += cookies;
   } 
   
   return totalCookies;
}

/*********************************************************************
 * calculateTotal contains all the math done in this program. It takes
 * the total number of boxes gotten in getCookies and multiplies
 * it by three point seven five and returns the total.
 * *******************************************************************/
float calculateTotal(float totalCookies)
{
   float totalIncome = 0;

   totalIncome = (totalCookies * 3.75);   
   
   return totalIncome;
}

/**********************************************************************
 * The functions displayTotal displays the final total calculated
 * in calculateTotal.
 * ********************************************************************/
void displayTotal(float totalIncome)
{
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);

   cout << "Total amount: $" << totalIncome << endl;
}
