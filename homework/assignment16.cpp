/***********************************************************************
* Program:
*    Assignment 16, Tax Bracket          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program takes the users income and retruns their tax bracket.
*
*    Estimated:  0.2 hrs   
*    Actual:     0.5 hrs
*      Relizing that I did still have to import the return value of
*      getIncome() otherwise the value is thrown away. despite being
*      passed into the function computePercent.
************************************************************************/

#include <iostream>
#include <iomanip>
using namespace std;

float getIncome();
float computePercent(float income);

/**********************************************************************
 * Main sets the return value of getIncome() to income, allowing use to 
 * use that value in the function otherwise the inputed value would be
 * thrown away once inputed. We also import the the value of that 
 * computePercent has found based on the user inputed value. Main then
 * returns the value decided upon by computePercent.
 ***********************************************************************/
int main()
{
   cout.setf(ios::fixed);
   cout.precision(0);

   float income = getIncome();
   float percent = computePercent(income);
   
   cout << "Your tax bracket is " << percent << "%" << endl;
   return 0;
}
 
/**********************************************************************
 * getIncome() prompts the user for the value of their income and 
 * returns that value.
 * ********************************************************************/
float getIncome()
{
   float income;

   cout << "Income: ";
   cin >> income;

   return income;
}

/*********************************************************************
* computePercent() takes the value of income from the function 
* getIncome() and decides which tax bracket the user should be paying
* in.
* ********************************************************************/
float computePercent(float income)
{
   float percent;

   if (income >= 0 && income < 15100)
      percent = 10;
   if (income >= 15100 && income < 61300)
      percent = 15;
   if (income >= 61300 && income < 123700)
      percent = 25;
   if (income >= 123700 && income < 188450)
      percent = 28;
   if (income >= 188450 && income < 336550)
      percent = 33;
   if (income >= 336550)
      percent = 35;

   return percent;
}
