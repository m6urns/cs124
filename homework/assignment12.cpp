/***********************************************************************
* Program:
*    Assignment 12, Input & Variables  (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program asks the user to input their monthly income, which is 
*    then reprinted onto the screen. 
*
*    Estimated:  1.0 hrs   
*    Actual:     0.5 hrs
*      Figuring how to to declare a variable was the hardest part of the 
*      assignment, also knowing where and if to place line terminations.
************************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

//Declare variable income
//float income;

/**********************************************************************
 * The function main outputs and waits for user input, input is placed 
 * in the variable income which is then output to the screen. 
 ***********************************************************************/
int main()
{
   float income;

   //Set cout options
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);
   
   //Output instructions
   cout << "\tYour monthly income: ";
   //Take user input and place into variable income
   cin >> income;
   //Print user inputed income end line
   cout << "Your income is: $" << setw(9) << income << endl;

   return 0;
}
