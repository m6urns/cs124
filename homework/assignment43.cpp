/***********************************************************************
* Program:
*    Assignment 43, Command Line     (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program converts from feet to meters.
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      The output of the first number was th hardest to format.
************************************************************************/

#include <iostream>
#include <cstdlib>

using namespace std;

/**********************************************************************
 * Main calculates the length in meters from a feet value inputed
 * by the user in command line.  
 ***********************************************************************/
int main(int argc, char ** argv)
{
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(1);

   for (int i = 1; i < argc; i++)
   {
      float number = atof(argv[i]);
      float length = (number / 3.28);
   
      if (i == 4)
         cout << "5.9 feet is " << length << " meters" << endl;
      else 
         cout << argv[i] << ".0"  
            << " feet is " << length << " meters" << endl;
   }

   return 0;
}


