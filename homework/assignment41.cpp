/***********************************************************************
* Program:
*    Assignment 41, Allocating Memory (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    Enter a brief description of your program here!
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
using namespace std;

int getChar();
void getText();
void printText();

/**********************************************************************
 * 
 ***********************************************************************/
int main()
{
   
   return 0;
}

int getChar()
{

   return char;
}

void getText()
{

   return;
}

void printText()
{

   return;
}
