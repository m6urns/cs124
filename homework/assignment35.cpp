/***********************************************************************
* Program:
*    Assignment 35, Advanced Things      
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program tells the user what letter grade he deserves based on
*    the inputed percentage.
*
*    Estimated:  0.75 hrs   
*    Actual:     1.0 hrs
*      This program didn't seem to work very well on setting the 
*      minus on grades, turns out I was setting and then removing it.
************************************************************************/

#include <iostream>
using namespace std;

/**********************************************************************
 * The function getNumberGrade gets the percent grade that the user
 * inputs. 
 ***********************************************************************/
int getNumberGrade()
{
   int gradeNumber = 0;

   cout << "Enter number grade: ";
   cin >> gradeNumber;

   return gradeNumber;  
}

/**********************************************************************
 * The function computeLetterGrade decides which letter grade the user
 * deserves and puts it into and array grade[]  
 ***********************************************************************/
char computeLetterGrade(char grade[], int gradeNumber)
{
    
   switch (gradeNumber / 10) 
   {
      case 10:
         grade[0] = 'A';
         break;
      case 9:
         grade[0] = 'A';
         break;
      case 8:
         grade[0] = 'B';
         break;
      case 7:
         grade[0] = 'C';
         break;
      case 6:
         grade[0] = 'D';
         break;
      case 5:
         grade[0] = 'F';
         break;
      default:
         grade[0] = 'F';
         break;        
   } 
}

/**********************************************************************
 * The function computeGradeSign computes the sign of the grade that the
 * user should get.  
 ***********************************************************************/
int computeGradeSign(char grade[], int gradeNumber)
{
   int firstNumber = (gradeNumber / 10);
   int baseGrade = (firstNumber * 10);
   int remainder = (gradeNumber - baseGrade);
 
   if (gradeNumber > 59 && gradeNumber < 90)
   {
      if (remainder <= 2)
         grade[1] = ((remainder <= 2) ? '-' : '\n');
      else if (remainder >= 7)
         grade[1] = ((remainder >= 7) ? '+' : '\n');
      else
         grade[1] = '\n';
   }
   else
   { 
      grade[1] = '\n';
   }
}

/**********************************************************************
 * Main calls funtions and holds information in arrays for various 
 * functions to modify.  
 ***********************************************************************/
int main()
{
   char grade[256];
   int  gradeNumber = getNumberGrade();

   computeLetterGrade(grade, gradeNumber);
   computeGradeSign(grade, gradeNumber);
   
   if (grade[1] == '\n')
      cout << gradeNumber << "% is " << grade[0] << grade[1];
   else 
      cout << gradeNumber << "% is " << grade[0] << grade[1] << endl;

   return 0;
}


