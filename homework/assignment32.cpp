/***********************************************************************
* Program:
*    Assignment 32, Strings          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program searchs a string for a letter of the users choice,
*    and tells the user how many times that letter occured.
*
*    Estimated:  0.5 hrs   
*    Actual:     1.0 hrs
*      Figuring out how to properly get the entire string took the
*      most time. 
************************************************************************/

#include <iostream>
using namespace std;

void getLetter(char letter[]);
void getString(char string[]);
int countLetters(char letter[], char string[]);
void displayNumber(char letter[], int count);

/**********************************************************************
 * Main creates two arrays that will be accesed by functions in the 
 * program. It calls these functions and calls the functions to display
 * the result. 
 ***********************************************************************/
int main()
{
   char letter[256];
   getLetter(letter);
   
   char string[256];
   getString(string);

   int count = countLetters(letter, string);

   displayNumber(letter, count);

   return 0;
}

/***********************************************************************
 * The function get letter gets the letter that the fucntion count 
 * letters will search for, it puts it in an array called letter.
 * *********************************************************************/
void getLetter(char letter[])
{
   cout << "Enter a letter: ";
   cin.getline(letter, 256);   
}

/***********************************************************************
 * The function get string gets the string you would like to search for
 * letters in and stores it to the array string.
 * *********************************************************************/
void getString(char string[])
{
   cout << "Enter text: ";
   cin.getline(string, 256);
}

/**********************************************************************
 * The function countLetters transverses the string and searchs for
 * a match to letter. If it find one is increments the count and 
 * returns the value of count once you reach the end of the string.
 * ********************************************************************/
int countLetters(char letter[], char string[])
{
   int count = 0;
   
 
   for (int i = 0; string[i]; i++)
   {
      if (string[i] == letter[0]) 
         count++;
   }

   return count;
}

/**********************************************************************
 * Display number displays the number of times that letter occured in
 * string as well as what letter was.
 * ********************************************************************/
void displayNumber(char letter[], int count)
{
   cout << "Number of " << "'" << letter[0] << "'s: " << count << endl;
}
