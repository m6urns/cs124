#include <iostream>
#include <iomanip>

using namespace std;

int getMonth();
int getYear();
bool leapYear(int year);
int yearTable(int year);
int monthTable(int month, int year);
int computeOffset(int month, int year);
void display(int offset, int month, int year);

int main()
{
   int year = getYear();
   int month = getMonth();
   int daysMonth = monthTable(month, year);
   int monthValue = monthTable(month, year);
   int offset = computeOffset(month, year);

   display(offset, month, year);

   return 0;
}

int getMonth()
{
   int month;
   
   cout << "Enter a month number: ";
   cin >> month;
   
   while (month < 1 || month > 12)
   {
      cout << "Month must be a number between 1 and 12." << endl;
      cout << "Enter a month number: ";
      cin >> month;
   }
   return month;
}

int getYear()
{
   int year;

   cout << "Enter year: ";
   cin >> year;

   while (year < 1753)
   {
      cout << "Year must be 1753 or later." << endl;
      cout << "Enter year: ";
      cin >> year;
   }

   return year;
}

bool leapYear(int year)
{
   return (year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);
}
int  yearTable(int year)
{
   int yearValue;

   if (leapYear(year) == true)
      yearValue = 366;
   else 
      yearValue = 365;

   return yearValue;
}

int monthTable(int month, int year)
{
   int monthValue;

   //January
   if (month == 1)
      monthValue = 31;
   //Febuary
   else if (month == 2)
      if (leapYear(year) == true)
         monthValue = 29;
      else 
         monthValue = 28;
   //March
   else if (month == 3)
      monthValue = 31;
   //April
   else if (month == 4)
      monthValue = 30;
   //May
   else if (month == 5)
      monthValue = 31;
   //June
   else if (month == 6)
      monthValue = 30;
   //July
   else if (month == 7)
      monthValue = 31;
   //August
   else if (month == 8)
      monthValue = 31;
   //September
   else if (month == 9)
      monthValue = 30;
   //October
   else if (month == 10)
      monthValue = 31;
   //November
   else if (month == 11)
      monthValue = 30;
   //December
   else if (month == 12)
      monthValue = 31;
   else 
      cout << "Error in monthTable (Valid 1-12): " << month << endl;

   return monthValue; 
}

int computeOffset(int month, int year)
{
   int offset = 0;
   int days;

   for (int count = 1753; count < year; count++)
      days += yearTable(count);

   for (int count = 1; count < month; count++)
      days += monthTable(count, year);

   offset = (days % 7);

   return offset;
}

void display(int offset, int month, int year)
{
   int day;
   int monthValue = monthTable(month, year);
   int numDays;

   if (month == 1)
      cout << "January";
   else if (month == 2)
      cout << "Febuary";
   else if (month == 3)
      cout << "March";
   else if (month == 4)
      cout << "April";
   else if (month == 5)
      cout << "May";
   else if (month == 6)
      cout << "June";
   else if (month == 7)
      cout << "July";
   else if (month == 8)
      cout << "August";
   else if (month == 9)
      cout << "September";
   else if (month == 10)
      cout << "October";
   else if (month == 11)
      cout << "November";
   else if (month == 12)
      cout << "December";
   else 
      cout << "Error in display (Valid Month 1-12): " << month << endl;

   cout << ", " << year << endl;
   cout << "  Su  Mo  Tu  We  Th  Fr  Sa" << endl;
   
   //Monday
   if (offset == 0)
   {
      day = 2;
      cout << setw(6);
   }
   //Tuesday
   else if (offset == 1)
   {
      day = 3;
      cout << setw(10);
   }
   //Wednesday
   else if (offset == 2)
   {
      day = 4;
      cout << setw(14);
   }
   //Thursday
   else if (offset == 3)
   {
      day = 5;
      cout << setw(18);
   }
   //Friday
   else if (offset == 4)
   {
      day = 6;
      cout << setw(22);
   }
   //Saturday
   else if (offset == 5)
   {
      day = 7;
      cout << setw(26);
   }
   //Sunday
   else if (offset == 6)
   {
      day = 1;
      cout << setw(2);
   }
   else 
      cout << "Error in display (Valid offset 0-6): " << offset << endl;

   for (int count = 1; count <= monthValue; count++)
   {
      cout << "  " << setw(2) << count;
      ++day;
      if (day == 8)
      {
         cout << endl;
         day = 1;
      }
   }
   if (day >= 2 && day <= 7)
      cout << endl;
}
