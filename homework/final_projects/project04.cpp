/***********************************************************************
* Program:
*    Project 04, Monthly Budget      (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program prompts the user to input information about their 
*    monthly budget. It then does nothing with the information except 
*    for put it in a nice viewable format for the user. This program 
*    has an implementation of the tax function, and passes testbed. 
*
*    Estimated:  3.0 hrs   
*    Actual:     2.5 hrs
*      Getting testBed to like the way I formatted everything. It still 
*      doesn't.
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

int display(float income, float budgetLiving, float actualLiving, 
   float actualTax, float actualTithing, float actualOther);
float getIncome();
float getBudgetLiving();
float getActualLiving();
float getActualOther();
float getActualTithing();
float getActualTax();
float computeTithing(float income);
float computeTax(float income);

/**********************************************************************
* The function main output the title of the program and calls the 
* function display() where the magic happens. Nothing much happens
* in poor old main, it just is an opener and a closer, for display().
************************************************************************/
int main() 
{   
   cout << "This program keeps track of your monthly budget" << endl;
   cout << "Please enter the following:" << endl;
 
   //Import variables from functions  
   float income = getIncome();
   float budgetLiving = getBudgetLiving();
   float actualLiving = getActualLiving();
   float actualTax = getActualTax();
   float actualTithing = getActualTithing();
   float actualOther = getActualOther();
    
   //Call display function display gets to use these vaiables 
   //because they were imported here. 
   //main > display > output variables
   display(income, budgetLiving, actualLiving, 
      actualTax, actualTithing, actualOther);

   return 0;
}

/**********************************************************************
*The function display() is where the magic happens in this prgram
*it displays all information stored outside of the function.
************************************************************************/
int display(float income, float budgetLiving, float actualLiving, 
   float actualTax, float actualTithing, float actualOther) 
{
   //When importing function into function now var type dec needed
   float budgetTax = computeTax(income);
   float budgetTithing = computeTithing(income);
   float budgetOther =
      (income - budgetTax - budgetTithing - budgetLiving);
   
   //Diffrences declared last to take variables inot account
   float budgetDiffrence = 
      (income - budgetTax - budgetTithing - budgetLiving - budgetOther);
   float actualDiffrence = 
      (income - actualTax - actualTithing - actualLiving - actualOther);
   
   //Set c.out options
   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2); 
  
   //Setup top info
   cout << "\n";
   cout << "The following is a report on your monthly expenses" << endl;
   cout << "\t" << "Item                  Budget          Actual" << endl;
   cout << "\t" << "=============== =============== ===============" << endl;
   //Table output
   cout << "\t" << "Income          $" << right << setw(11) << income 
      << "    $" << right << setw(11) <<  income << endl;
   cout << "\t" << "Taxes           $" << right << setw(11) << budgetTax 
      << "    $" << right << setw(11) << actualTax << endl;
   cout << "\t" << "Tithing         $" << right << setw(11) << budgetTithing 
      << "    $" << right << setw(11) << actualTithing << endl;
   cout << "\t" << "Living          $" << right << setw(11) << budgetLiving 
      << "    $" << right << setw(11) << actualLiving << endl;
   cout << "\t" << "Other           $" << right << setw(11) << budgetOther 
      << "    $" << right << setw(11) << actualOther << endl;
   //Diffrences
   cout << "\t" << "=============== =============== ===============" << endl;
   cout << "\t" << "Difference      $" << right << setw(11) << budgetDiffrence 
      << "    $" << right << setw(11) << actualDiffrence << endl;
}

/**********************************************************************
*The function getIncome() takes the monthly income of the user
*and stores it to the variable income, and returns income.
***********************************************************************/
float getIncome() 
{ 
   
   float income;

   cout << "\tYour monthly income: ";
   cin >> income;

   return income;
}

/**********************************************************************
*The function getBudgetLiving() takes the users budgeted living
*monthly living expenses and returns the value in budgetLiving.
***********************************************************************/
float getBudgetLiving() 
{

   float budgetLiving;
   
   cout << "\tYour budgeted living expenses: ";
   cin >> budgetLiving;

   return budgetLiving;
}

/**********************************************************************
*The function getActualLiving takes the users actual living expenses
*and returns them as actualLiving.
***********************************************************************/
float getActualLiving() 
{
    
   float actualLiving;
 
   cout << "\tYour actual living expenses: ";
   cin >> actualLiving;

   return actualLiving;
}

/**********************************************************************
*The function getActualOther get the other expenses of the user
*and returns the value in actualOther.
***********************************************************************/
float getActualOther() 
{

   float actualOther;
  
   cout << "\tYour actual other expenses: ";
   cin >> actualOther;

   return actualOther;
}

/**********************************************************************
*The function getActualTithing gets the users actual tithing paid
*and returns the value as actualTithing.
***********************************************************************/
float getActualTithing() 
{
   
   float actualTithing;

   cout << "\tYour actual tithe offerings: ";
   cin >> actualTithing;

   return actualTithing;
}

/**********************************************************************
*The function getActualTax get the value the user paid in tax per 
* month it returns the value as actualTax.
***********************************************************************/
float getActualTax() 
{

   float actualTax;
 
   cout << "\tYour actual taxes withheld: ";
   cin >> actualTax;

   return actualTax;
}

/**********************************************************************
 * The function computeTithing computes the users actual owed tithing
 * by dividing income by 10. The function imports income from getIncome
 * and divides the value by ten. returns the true value of the tithing
 * the user should be paying based on income.
 * ********************************************************************/
float computeTithing(float income) 
{

   float tithing = (income / 10.0);

   return tithing;    
}

/**********************************************************************
 * The function compute tax computes the users monthly tax burden
 * for some reason it does not like to really compute taxes for input
 * under 15100 $. But does enjoy computing taxes for the rich.
 * ********************************************************************/
float computeTax(float income)
{
   //Find users yearly income   
   float yearlyIncome = (income * 12.0);
   //Calculate users taxes
   float yearlyTax;
   
   //Tier 1 0 - 15100
   if (yearlyIncome >= 0.0 && yearlyIncome < 15100.0) 
      yearlyTax = (0.0 + (0.10 * (yearlyIncome - 0.0 )));
   //Tier 2 15100 - 61300
   if (yearlyIncome >= 15100.0 && yearlyIncome < 61300.0)
      yearlyTax = (1510.0 + (0.15 * (yearlyIncome - 15100.0)));
   //Tier 3 61300 - 123700
   if (yearlyIncome >= 61300.0 && yearlyIncome < 123700.0)
      yearlyTax = (8440.0 + (0.25 * (yearlyIncome - 61300.0)));
   //Tier 4 123700 - 188450
   if (yearlyIncome >= 123700.0 && yearlyIncome < 188450.0)
      yearlyTax = (24040.0 + (0.28 * (yearlyIncome - 123700.0)));
   //Tier 5 188450 - 336550
   if (yearlyIncome >= 188450.0 && yearlyIncome < 336550.0)
      yearlyTax = (42170.0 + (0.33 * (yearlyIncome - 188450.0)));
   //Tier 6 336550 - infinity
   if (yearlyIncome >= 336550.0)
      yearlyTax = (91043.0 + (0.35 * (yearlyIncome - 336550.0)));

   //Find users monthly tax
   float monthlyTax = (yearlyTax / 12.0);
   
   return monthlyTax;
}
