/******************************************************************
* Program: 
*    Project 07, Calender
*    Brother Falin, CS124
* Author: 
*    Matthew Burns
* Summary: 
*    This program displays a calender based on user input. It 
*    can display a calender for any year from 1753 onward.
*
*    Estimated: 1.0
*    Actual: 3.0
*       The hardest part of creating this program was getting the 
*       offset to function correctly. I had an unintilaized 
*       variable that made everything act randomly. 
*
* ****************************************************************/

#include <iostream>
#include <iomanip>

using namespace std;

int getMonth();
int getYear();
bool leapYear(int year);
int yearTable(int year);
int monthTable(int month, int year);
int computeOffset(int month, int year);
void display(int offset, int month, int year);

/******************************************************************
 * Main calls variables and passes data between functions. 
 * It calls the display function once the calculations
 * have been completed.
 * ****************************************************************/
int main()
{
   int month = getMonth();
   int year = getYear();
   int offset = computeOffset(month, year);

   display(offset, month, year);

   return 0;
}

/******************************************************************
 * Get month gets the month the user wishes to calculate for 
 * it also check the input to make sure its valid.
 * ****************************************************************/
int getMonth()
{
   int month;
   
   cout << "Enter a month number: ";
   cin >> month;
   
   while (month < 1 || month > 12)
   {
      cout << "Month must be between 1 and 12." << endl;
      cout << "Enter a month number: ";
      cin >> month;
   }
   return month;
}

/******************************************************************
 * getYear get the year the user wishes to calculate for, it 
 * checks this year to assure it is valid.
 * ****************************************************************/
int getYear()
{
   int year;

   cout << "Enter year: ";
   cin >> year;

   while (year < 1753)
   {
      cout << "Year must be 1753 or later." << endl;
      cout << "Enter year: ";
      cin >> year;
   }

   return year;
}

/******************************************************************
 * The leapYear function returns true or false based on whether 
 * or not the year is a leap year.
 * ****************************************************************/
bool leapYear(int year)
{
   return (year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);
}

/******************************************************************
 * The year table holds the values of both leap years and non leap
 * years. This information is used in calculation of the offset.
 * ****************************************************************/
int  yearTable(int year)
{
   int yearValue;

   if (leapYear(year) == true)
      yearValue = 366;
   else 
      yearValue = 365;

   return yearValue;
}

/******************************************************************
 * The monthTable holds the value of each month of the year 
 * this information is used both by the offset calculation
 * but also by the display function.
 * ****************************************************************/
int monthTable(int month, int year)
{
   int monthValue;

   //January
   if (month == 1)
      monthValue = 31;
   //February
   else if (month == 2)
      if (leapYear(year) == true)
         monthValue = 29;
      else 
         monthValue = 28;
   //March
   else if (month == 3)
      monthValue = 31;
   //April
   else if (month == 4)
      monthValue = 30;
   //May
   else if (month == 5)
      monthValue = 31;
   //June
   else if (month == 6)
      monthValue = 30;
   //July
   else if (month == 7)
      monthValue = 31;
   //August
   else if (month == 8)
      monthValue = 31;
   //September
   else if (month == 9)
      monthValue = 30;
   //October
   else if (month == 10)
      monthValue = 31;
   //November
   else if (month == 11)
      monthValue = 30;
   //December
   else if (month == 12)
      monthValue = 31;
   else 
      cout << "Error in monthTable (Valid 1-12): " << month << endl;

   return monthValue; 
}

/******************************************************************
 * computeOffset calculates the number of days which have passed
 * since 1753. It calculates the remainder of this total which 
 * is the offset for the display.
 * ****************************************************************/
int computeOffset(int month, int year)
{
   int offset = 0;
   int daysPassed = 0;

   for (int count = 1753; count < year; count++)
      daysPassed += yearTable(count);

   for (int count = 1; count < month; count++)
      daysPassed += monthTable(count, year);

   offset = (daysPassed % 7);

   return offset;
}

/******************************************************************
 * The display function takes all the information found by the
 * other functions and puts it to use displaying a useful 
 * month calender for the user. 
 * ****************************************************************/
void display(int offset, int month, int year)
{
   int day;
   int monthValue = monthTable(month, year);

   cout << endl;

   if (month == 1)
      cout << "January";
   else if (month == 2)
      cout << "February";
   else if (month == 3)
      cout << "March";
   else if (month == 4)
      cout << "April";
   else if (month == 5)
      cout << "May";
   else if (month == 6)
      cout << "June";
   else if (month == 7)
      cout << "July";
   else if (month == 8)
      cout << "August";
   else if (month == 9)
      cout << "September";
   else if (month == 10)
      cout << "October";
   else if (month == 11)
      cout << "November";
   else if (month == 12)
      cout << "December";
   else 
      cout << "Error in display (Valid Month 1-12): " << month << endl;

   cout << ", " << year << endl;
   cout << "  Su  Mo  Tu  We  Th  Fr  Sa" << endl;
   
   //Monday
   if (offset == 0)
   {
      day = 2;
      cout << setw(6);
   }
   //Tuesday
   else if (offset == 1)
   {
      day = 3;
      cout << setw(10);
   }
   //Wednesday
   else if (offset == 2)
   {
      day = 4;
      cout << setw(14);
   }
   //Thursday
   else if (offset == 3)
   {
      day = 5;
      cout << setw(18);
   }
   //Friday
   else if (offset == 4)
   {
      day = 6;
      cout << setw(22);
   }
   //Saturday
   else if (offset == 5)
   {
      day = 7;
      cout << setw(26);
   }
   //Sunday
   else if (offset == 6)
   {
      day = 1;
      cout << setw(2);
   }
   else 
      cout << "Error in display (Valid offset 0-6): " << offset << endl;

   for (int count = 1; count <= monthValue; count++)
   {
      cout << "  " << setw(2) << count;
      ++day;
      if (day == 8)
      {
         cout << endl;
         day = 1;
      }
   }
   if (day >= 2 && day <= 7)
      cout << endl;
}
