/***********************************************************************
* Program:
*    Project 06, Calander Offset      (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This program calculates the offset for the calander program. 
*
*    Estimated:  3.0 hrs
*    Actual:     2.5 hrs
*       The hardest part of this program was getting offset to actully 
*       calculate the correct offset. 
* ************************************************************************/

#include <iostream>

using namespace std;

int getMonth();
int getYear();
int monthTable(int month, int year);
int yearTable(int year);
bool leapYear(int year);
int computeOffset(int month, int year);
int display(int offset);

/**************************************************************************
 * Main calls functions and executes them. It then calls the display 
 * function and prints the offset.
 * ************************************************************************/
int main()
{
   int month = getMonth();
   int year =  getYear();

   int offset = computeOffset(month, year);
   display(offset); 
}

/**************************************************************************
 * Month get the month value from the user.
 * ************************************************************************/
int getMonth()
{
   int month;

   cout << "Enter a month number: ";
   cin >> month;
   
   while (month < 1 || month > 12)
   {
      cout << "Month must be between 1 and 12." << endl;
      cout << "Enter a month number: ";
      cin >> month;
   }
   return month;
}

/**************************************************************************
 * This function gets the year from the user.
 * ************************************************************************/
int getYear()
{
   int year;
   
   cout << "Enter year: ";
   cin >> year;
   
   while (year < 1753)
   {
      cout << "Year must be 1753 or later." << endl;
      cout << "Enter year: ";
      cin >> year;
   }

   return year;
}

int yearTable(int year)
{
   int yearValue;

   if (leapYear(year) == true)
      yearValue = 366;
   else 
      yearValue == 365;

   return yearValue;
}
/**************************************************************************
 * Compute offset computes the offset for the given month and year
 * .... hopefully.
 * ************************************************************************/
int computeOffset(int month, int year)
{
   int offset = 0;
   int days;
   
   for (int count = 1753; count < year; count++)
      //numDays += daysInYear(count);
      days += yearTable(count);

   for (int count = 1; count < month; count++)
      //numDays += daysInMonth(count, year);
      days += monthTable(count, year);

   offset = (days % 7);

   return offset;
}

/**************************************************************************
 * leapYear finds if a year is aleap year or not.
 * ************************************************************************/
bool leapYear(int year)
{
   return (year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);
}

/**************************************************************************
 * This table holds the number of days in each month of a year.
 * ************************************************************************/
int monthTable(int month, int year)
{
   int monthValue;
   
   //January  
   if (month == 1)
      monthValue = 31;
   //Febuary
   else if (month == 2)
   {
      if (leapYear(year) == true)
         monthValue = 29;
      else 
         monthValue = 28;
   }
   //March
  else if (month == 3)
      monthValue = 31;
   //April
   else if (month == 4)
      monthValue = 30;
   //May
   else if (month == 5)
      monthValue = 31;
  //June
   else if (month == 6)
      monthValue = 30;
  //July
   else if (month == 7)
      monthValue = 31;
   //August
   else if (month == 8)
      monthValue = 31;
   //September
   else if (month == 9)
      monthValue = 30;
   //October
   else if (month == 10)
      monthValue = 31;
   //November
   else if (month == 11)
      monthValue = 30;
   //December
   else if (month == 12)
      monthValue = 31;

   return monthValue;
}

/**************************************************************************
 * This function displays the offset.
 * ************************************************************************/
int display(int offset)
{
   cout << "Offset: " << offset << endl;

   return 0;
}

