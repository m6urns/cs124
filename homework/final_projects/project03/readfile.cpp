/************************************************************************
 * Program:
 *    Project 09, Mad Lib      (e.g. Assignment 01, Hello World)  
 *    Brother Falin, CS124
 * Author:
 *    Matthew Burns
 * Summary: 
 *    This is supposed to be the Mad Lib Project, it is not in a state 
 *    even close to being considered this project. My submission is late 
 *    and I can't figure this thing out, this is what  I have. 
 *
 *    Estimated:  5.0 hrs   
 *    Actual:     12.0 hrs
 *      I can't figure out how to prompt users for input correctly, the 
 *      entire thing doesn't work very well.
 ************************************************************************/
#include <iostream>
#include <fstream>

using namespace std;
 
#define SIZE 256

/*************************************************************************
 * The get fileName function gets the name of the file you wish to process
 * from the user.
 * ***********************************************************************/  
void getFileName(char fileName[])
{
   cout << "Please enter the filename of the Mad Lib: ";
   cin >> fileName;
   return;
}

/*************************************************************************
 * Readfile reads the contents of the specified file, it removes marks and
 * should prompt the user for the value of some prompts. Right now it only
 * removes things from the text.
 * ***********************************************************************/
void readFile(char fileName[], char file[][SIZE])
{
   ifstream fin(fileName);
   if (fin.fail())
      return;
   
   int count = 0;
   char word[SIZE];

   while (fin >> word)
   {
      if (word[0] == '<')
      {
         if (word[0] == '<')
            word[0] = ' ';

         if (word[1] == '#')
            word[1] = '\n';
         if (word[1] == '{')
            word[1] = '\"';
         if (word[1] == '}')
            word[1] = '\"';
         if (word[1] == '[')
            word[1] = '\'';
         if (word[1] == ']')
            word[1] = '\''; 

         if (word[2] == '>')
            word[2] = ' ';
         
         else 
         {
            for (int i = 1; word[i]; i++)
               if (word[i] == '_')
                  word[i] = ' ';
         }
      }
      cout << word << " ";   
      count++;
   }

   fin.close();

   return;
}

/*********************************************************************
 * Main holds arrays and passes them to other functions to be worked on.
 * *******************************************************************/
int main()
{
   char fileName[SIZE];
   char file[SIZE][SIZE];

   getFileName(fileName);
   readFile(fileName, file);

   return 0;
}
