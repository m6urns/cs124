/************************************************************************
* Program:
*    Project 09, Mad Lib      (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This program prompts users for the answer to a question that is read 
*    from the mad lib file.
*
*    Estimated:  5.0 hrs
*    Actual:     16.0 hrs
*       This program now passes testBed, after a long struggle with just
*       about everything in it. 
************************************************************************/
#include <iostream>
#include <fstream>

using namespace std;

void askQuestion(char word[]);
bool isQuestion(char word[]);

#define SIZE 256

/***********************************************************************
 * The functions getFileNAme gets the name of the file from the user
 * and passes it on for other functions to use.
 * *********************************************************************/
void getFileName(char fileName[])
{
   cout << "Please enter the filename of the Mad Lib: ";
   cin >> fileName;

   cin.ignore();
 
   return;
}

/***********************************************************************
 * The function readfile reads a file and decides if is is nessecary 
 * to prompt for a question.
 * *********************************************************************/
void readFile(char fileName[], char file[][SIZE], char word[])
{
   ifstream fin(fileName);
   while (fin.fail())
      return;

   while (fin >> word)
   {
      if (isQuestion(word))
      {
         askQuestion(word);
      }
   }

   fin.close();

   return;
}

/***********************************************************************
 * The function askQuestion asks the user a question depending on what is
 * in the operators.
 * *********************************************************************/
void askQuestion(char word[])
{
   char answer[SIZE];
 
   for (int i = 0; word[i]; i++)
   {      
      if (word[i] == '<')
         word[i] = '\t';
      if (word[i] == '_')
         word[i] = ' ';
      if (word[i] == '>')
         word[i] = ':';
   }
 
   word[1] = (toupper(word[1]));  
   
   for (int i = 0; word[i]; i++)
   {
      cout << word[i];
   }
   
   cout << " ";
    
   cin.getline(answer, 256);

   return;
}

/***********************************************************************
 * The bool is Question tells the function readFile if an operator 
 * qualifies as a question or if it is some other type of
 * operator.
 * *********************************************************************/
bool isQuestion(char word[])
{
   if (word[0] == '<')
   {
      if (word[1] == '#')
         return false;
      if (word[1] == '{')
         return false;
      if (word[1] == '}')
         return false;
      if (word[1] == '[')
         return false;
      if (word[1] == ']')
         return false;
      else

         return true;
   }
   
   else 
      return false;
}

/***********************************************************************
 * The functions main holds the value of char variables passed between
 * functions.
 * *********************************************************************/
int main()
{
   char fileName[SIZE];
   char file[SIZE][SIZE];
   char word[SIZE];

   getFileName(fileName);
   readFile(fileName, file, word);

   cout << "Thank you for playing." << endl;
}
