/************************************************************************
* Program:
*    Project 09, Mad Lib      (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This program prompts users for the answer to a question that is read 
*    from the mad lib file.
*
*    Estimated:  5.0 hrs
*    Actual:     16.0 hrs
*       This program now passes testBed, after a long struggle with just
*       about everything in it. 
************************************************************************/
#include <iostream>
#include <fstream>

using namespace std;

void askQuestion(char word[]);
bool isQuestion(char word[]);
void modifer(char word[]);
bool isModifier(char word[]);

#define SIZE 256

/***********************************************************************
 * The functions getFileNAme gets the name of the file from the user
 * and passes it on for other functions to use.
 * *********************************************************************/
void getFileName(char fileName[])
{
   cout << "Please enter the filename of the Mad Lib: ";
   cin >> fileName;

   cin.ignore();
 
   return;
}

/***********************************************************************
 * The function readfile reads a file and decides if is is nessecary 
 * to prompt for a question.
 * *********************************************************************/
void readFile(char fileName[], char file[][SIZE], char word[])
{
   int count = 0;

   ifstream fin(fileName);
   if (fin.fail())
   {
      cout << "Error reading file: " << fileName << endl;
      return;
   }

   while (count < 256 && fin >> word)
   {
      if (isModifier(word));
      {
         modifer(word);
      }

      if (isQuestion(word))
      {
         askQuestion(word);
      }

      count++;      
   }

   fin.close();

   return;
}

/***********************************************************************
 * The function askQuestion asks the user a question depending on what is
 * in the operators.
 * *********************************************************************/
void askQuestion(char word[])
{
   char answer[SIZE];
 
   for (int i = 0; word[i]; i++)
   {      
      if (word[i] == '<')
         word[i] = '\t';
      if (word[i] == '_')
         word[i] = ' ';
      if (word[i] == '>')
         word[i] = ':';
   }
 
   word[1] = (toupper(word[1]));  
   
   for (int i = 0; word[i]; i++)
   {
      cout << word[i];
   }
   
   cout << " ";
    
   cin.getline(answer, 256);

   return;
}

void modifer(char word[])
{

   switch (word[1])
   {
      case '#':
         word[0] = '\n';
         word[1] = '\0';
         break;
      case '{':
         word[0] = ' ';
         word[1] = '\"';
         word[2] = '\0';
         break;
      case '}':
         word[0] = '\"';
         word[1] = ' ';
         word[2] = '\0';
         break;
      case '[':
         word[0] = ' ';
         word[1] = '\'';
         word[2] = '\0';
         break;
      case ']':
         word[0] = '\'';
         word[1] = ' ';
         word[2] = '\0'; 
         break;
   }

   return;
}
/***********************************************************************
 * The bool is Question tells the function readFile if an operator 
 * qualifies as a question or if it is some other type of
 * operator.
 * *********************************************************************/
bool isQuestion(char word[])
{
   if (word[0] == '<')
   {
      if (word[1] == '#')
         return false;
      if (word[1] == '{')
         return false;
      if (word[1] == '}')
         return false;
      if (word[1] == '[')
         return false;
      if (word[1] == ']')
         return false;
      else

         return true;
   }
   
   else 
      return false;
}

bool isModifier(char word[])
{
   if (word[1] == '<')
   {
      if (word[1] == '#')
         return true;
      if (word[1] == '{')
         return true;
      if (word[1] == '}')
         return true;
      if (word[1] == '[')
         return true;
      if (word[1] == ']')
         return true;
      else
      
         return false;
   }

   else
      return false;
}

void display(char file[][SIZE])
{
   for (int i = 0; i < SIZE; i++)
   {
      if (i == 0)
         cout << file[i];
      else if (file[i][0] = '.' || file[i][0] == ',')
         cout << file[i];
      else 
         cout << " " << file[i];
   }
   
   cout << endl;

   return;
}

/***********************************************************************
 * The functions main holds the value of char variables passed between
 * functions.
 * *********************************************************************/
int main()
{
   char fileName[SIZE];
   char file[SIZE][SIZE];
   char word[SIZE];
   
   char yesNo;
   bool play = true;
   
   while (play)
   {
      getFileName(fileName);
      readFile(fileName, file, word);
      display(file);     

      cout << "Do you want to play again (y/n)? ";
      cin >> yesNo;
      
      if (yesNo == 'n')
      {
         play = false;
         cout << "Thank you for playing." << endl;
      }
      else if (yesNo == 'y')
         play = true;
      else if (yesNo != 'y' || yesNo != 'n')
      {
         cout << "Invalid entry. Do you want to play again (y/n)? ";
         cin >> yesNo;
      }
   }

   return 0;
}
