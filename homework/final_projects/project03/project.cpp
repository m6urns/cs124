/***********************************************************************
* Program:
*    Project 10, Mad Lib          (e.g. Assignment 01, Hello World)
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary:
*    This program reads from a MadLib file. And gets input form the user
*    which is then inserted inot the file. The file is then printed.
*
*    Estimated:  5.0 hrs
*    Actual:     8.0 hrs
*      Printing the output correctly formatted was difficult
*      I also have an issue with a ! printing at the end of the 
*      output on one of the tests.
************************************************************************/

#include <iostream>
#include <fstream>

using namespace std;

#define SIZE 256 

void getFileName(char fileName[]);
int readFile(char madLib[][SIZE]);
void askQuestions(char prompt[], int count);
void getPunctuation(char punc[]);
void display (char madLib[][SIZE], int numWords);

/***********************************************************************
 * The main hold the data from the file in a char, which is used 
 * by many of the functions.
 ***********************************************************************/
int main()
{
   char madLib[SIZE][SIZE];
   int numWords;

   char yesNo;
   bool play = true;

   while (play)
   {
      numWords = readFile(madLib);
      cout << endl; 
      display(madLib, numWords);

      cout << endl;
      cout << "Do you want to play again (y/n)? ";
      cin >> yesNo;

      if (yesNo == 'n')
      {
         play = false;
         cout << "Thank you for playing." << endl;
      }
      else if (yesNo == 'y')
         play = true;

      else if (yesNo != 'y' || yesNo != 'n')
      {
         cout << "Invalid entry.  Do you want to play again (y/n)? ";
         cin >> yesNo;
      }  
   }   
}

/***********************************************************************
 * This functions gets the filename that the user would like
 * to open.
 ***********************************************************************/
void getFileName(char fileName[])
{
   cout << "Please enter the filename of the Mad Lib: ";
   cin >> fileName;
}

/***********************************************************************
 * The readfile function checks for errors and check the file for
 * modifiers, passing it to other functions for data processing.
 ***********************************************************************/
int readFile(char madLib[][SIZE])
{
   char fileName[256];
   getFileName(fileName);

   ifstream fin(fileName);

   if (fin.fail())
   {
      cout << "Error reading file: " << fileName << endl;
      return -1; 
   }

   int count = 0;
   int numWords = 0;
   while (numWords < SIZE && fin >> madLib[numWords])
   {
      if (madLib[numWords][0] == '<' && isalpha(madLib[numWords][1]))
      {
         askQuestions(madLib[numWords], count);
         count++;
      }
      else if (madLib[numWords][0] == '<' && !isalpha(madLib[numWords][2]))
         getPunctuation(madLib[numWords]);
      numWords++;
   }
   
   fin.close();
   return numWords;
   
}

/***********************************************************************
 * The ask questions function asks the user questions based on the
 * file. It then passes the input to the char.
 ***********************************************************************/
void askQuestions(char text[], int count)
{
   cout << "\t" << (char)toupper(text[1]);

   for (int i = 2; text[i]; i++)
   {  
      if (text[i] == '<')
         text[i] = '\t';
      if (text[i] == '_')
         text[i] = ' ';
      if (text[i] == '>')
         text[i] = ':';
      else 
      {
         cout << (char)tolower(text[i]);
      }
        
   }

   cout << ": ";                                    

   if (count == 0)
   {
      cin.ignore();
      cin.getline(text,256);
   }
   else if (count > 0)
      cin.getline(text,256);
   
   return;
}

/***********************************************************************
 * The get punctuation functions decides how to punctuate the
 * modifiers in the file, it passes this information back to 
 * the functions readFile.
 ***********************************************************************/
void getPunctuation(char punc[])
{   

   {
      switch (punc[1])
      {
         case '#':
            punc[0] = '\n';
            punc[1] = '\0';
            break;
         case '{':
            punc[0] = ' ';
            punc[1] = '\"';
            punc[2] = '\0';
            break;
         case '}':
            punc[0] = '\"';
            punc[1] = ' ';
            punc[2] = '\0';
            break;
         case '[':
            punc[0] = ' ';
            punc[1] = '\'';
            punc[2] = '\0';
            break;
         case ']':
            punc[0] = '\'';
            punc[1] = ' ';
            punc[2] = '\0';
            break;
      }
         
   }
   
   return; 
}

/***********************************************************************
 * The display functions decides how to print the contents of the char
 * array madLib.
 ***********************************************************************/
void display (char madLib[][SIZE], int numWords)   
{
   for (int i = 0; i < (numWords); i++)
   {  
      if (i == 0)
         cout << madLib[i];

      //Front Quote Marks
      else if (madLib[i][1] == '"')b
      {
         if (madLib[i][1] == '"')
         {
            cout << " \"";
            i++;
            cout << madLib[i];
         }
         else 
            cout << madLib[i];
      }

      //Front Single Quotes   
      else if (madLib[i][1] == '\'')
      {
         if (madLib[i][1] == '\'')
         {  
            cout << " \'";
            i++;
            cout << madLib[i];
         }
         else  
            cout << madLib[i];
      }

      //Newlines
      else if (madLib[i][0] == '\n')
      {
         cout << "\n";
         i++;
         if (madLib[i][0] == '\n')
         {
            //madLib[i][0] = '\n';
            cout << "\n";
            i++;
            //numWords++;
            cout << madLib[i];
         }
         else 
            cout << madLib[i];     
      }

         
      //Back Double Quotes   
      else if (madLib[i][0] == '"')
      {
         if (madLib[i][1] == ' ')
            cout << "\"";
      }

      //Back Single Quotes
      else if (madLib[i][0] == '\'')
      {
         if (madLib[i][1] == ' ')
            cout << "\'";
      }

      //Other punctuation        
      else if (madLib[i][0] == '.' || madLib[i][0] == ',' 
            || madLib[i][0] == '?' || madLib[i][0] == '!')
         cout << madLib[i];
         
      else  
         cout << " " << madLib[i];
   }
}
