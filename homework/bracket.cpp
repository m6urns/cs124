/***********************************************************************
* Program:
*    Assignment ##, ????          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    Enter a brief description of your program here!
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
using namespace std;

int getPlayers();
int getRounds();
int playersPerRound(int numberPlayers, int numberRounds);
int bracket(int numberPlayer, int numberRounds, int perRound);

/**********************************************************************
 * 
 ***********************************************************************/
int main()
{
   int numberPlayers = getPlayers();
   int numberRounds = getRounds();
   int perRound = playersPerRound(numberPlayers, numberRounds);

   bracket(numberPlayers, numberRounds, perRound);

   return 0;
}

int getPlayers()
{
   int numberPlayers = 0;

   cout << "Enter players in each round: ";
   cin >> numberPlayers;

   return numberPlayers;
}

int getRounds()
{
   int numberRounds = 0;

   cout << "Enter number of rounds: ";
   cin >> numberRounds;

   return numberRounds;
}

int playersPerRound(int numberPlayers, int numberRounds)
{
   int perRound = 0;

   //cout << "Players per round: ";
   //cin >> perRound;

   return perRound;
}

int bracket(int numberPlayers, int numberRounds, int perRound)
{
   int players[numberPlayers];
   int number = 0;
   int score = 0;
   
   
   //Set slots equal to zero   
   for (int i = 0; players[i]; i++)
   {
      players[i] = 0;
   }
     
   for (int i = 0; i < numberRounds; i++)
   {
      number = (i + 1);
      cout << "Round " << number << endl;
   
      for (int i = 0; i < numberPlayers; i++)
      {
         int score = 0;

         number = (i + 1);
      
         cout << "\t" << "Player " << number << ": ";
         cin >> score;

         players[i] += score;
      }
   }
   
   cout << "Final Scores" << endl;

   for (int i = 0; players[i]; i++)
   {
      number = (i + 1);
   
      cout << "\t" << "Player " << number << ": " << players[i] << endl;
   }   
}
