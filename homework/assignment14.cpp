/***********************************************************************
* Program:
*    Assignment 14, Matthew 18:21-22
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program will address Peter's question to the Lord in 
*    Matthew 18:21-22
*
*    Estimated:  0.5 hrs   
*    Actual:     0.2 hrs
*      You have to create functions in descending order of there use
*      or initiate them before main().
************************************************************************/

#include <iostream>
using namespace std;

void questionPeter();
int responseLord();

/**********************************************************************
* You are not allowed to change MAIN in any way; just implement the two
* functions: questionPeter() and responseLord()
***********************************************************************/
int main()
{
   // ask Peter's question
   questionPeter();

   // the first part of the Lord's response
   cout << "Jesus saith unto him, I say not unto thee, Until seven\n";
   cout << "times: but, Until " << responseLord() << ".\n";

   return 0;
}


/***********************************************************************
 * This function output Peter's question
 ***********************************************************************/
void questionPeter()
{
   cout << "Lord, how oft shall my brother sin against me, and I forgive him?" 
      << endl;
   cout << "Till seven times?" << endl;

   return;
}

/***********************************************************************
 * This function gives the Lord's response (490)
 ***********************************************************************/ 
int responseLord()
{
   int responseLord = 7 * 70;
   
   return responseLord;
}


//int main() {

  // int x = getANumber();
  // int y = getANumber();
//}

//int getANumebr() { 

  // int theNumber;
  // cout << "Enter number";
  // cin >> theNumber;
   
  // return theNumber;
//}
   
