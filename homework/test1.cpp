/**************************************************
* Program:
*    Test 1, Conversion
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program converts US dollars to European Euros.
*    Including large numbers. It uses double variables
*    to handle these large numbers.
************************************************************/
#include <iostream>
using namespace std;

/**************************************************
* The function main take user input and converts it to 
* a Euro value, it then outputs that value for the user.
* In the event of a negative value the program uses 
* () around negatives.
* ***************************************************/
int main() 
{

   cout.setf(ios::fixed);
   cout.setf(ios::showpoint);
   cout.precision(2);
   
   double dollars;
   
   cout << "Please enter the amount in US Dollars: $";
   cin >> dollars;
   
   double euros = (dollars / 1.4100);

   if (euros >= 0)
      cout << "\tEuros: " << euros << endl;
   else 
      cout << "\tEuros: " << "(" << euros << ")" << endl;

   return 0;
}

