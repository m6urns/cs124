/***********************************************************************
* Program:
*    Assignment 02, Monthly Budget      (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program prompts the user to input information about their 
*    monthly budget. It then does nothing with the information except 
*    for put it in a nice viewable format for the user. 
*
*    Estimated:  3.0 hrs   
*    Actual:     2.5 hrs
*      Getting testBed to like the way I formatted everything. It still 
*      doesn't.
************************************************************************/
#include <iomanip>
#include <iostream>
using namespace std;

float yourRoundOne;
float yourRoundTwo;
float dateRoundOne;
float dateRoundTwo;
//int yourTotal = (yourRoundOne + yourRoundTwo);
//int dateTotal = (dateRoundOne + dateRoundTwo);

/**********************************************************************
 * The function main prompts the user to input information about 
 * their budget, it then takes that information and formats it
 * nicely for the users enjoyment. 
 ***********************************************************************/
int main()
{

   cout.setf(ios::fixed);
   cout.precision(0);
   
   //Input your expenses
   cout << "Super Awesome Minigolf Scorecard Program" << endl;
   cout << "Please enter the following:" << endl;
   cout << "\tYour score for round 1: ";
   cin >> yourRoundOne;
   cout << "\tYour score for round 2: ";
   cin >> yourRoundTwo;
   cout << "\tYour date's score for round 1: ";
   cin >> dateRoundOne;
   cout << "\tYour date's score for round 2: ";
   cin >> dateRoundTwo;   
   //Setup top info
   cout << endl;
   cout << "Here is your minigolf scorecard" << endl;
   cout << "\t" << left << setw(15) << " " << right << setw(13) << "You" << right << setw(16) << "Your Date" << endl;
   cout << "\t" << left << setw(16) << "===============" << left << setw(16) << "===============" << left << setw(15) << "===============" << endl;
   //Output your formatted budget tab in left align 16 spaces and print 
   //Income within that space left aligned. Dollar sign at 17 spaces from
   //beginning. 15+1=16 17 is starting space for first info column. 
   //Set a space of 11 to print income var we want to leave four spaces 
   //at the end of the output. Print another dollar sign five spaces over. 
   //give final var 11 spaces to print. 
   cout << "\t" << left << setw(16) << "Round 1" << right << setw(12) << yourRoundOne << right <<  setw(16) << dateRoundOne << endl;
   cout << "\t" << left << setw(16) << "Round 2" << right << setw(12) << yourRoundTwo << right << setw(16) << dateRoundTwo << endl;
   //Setup bottom info
   cout << "\t" << left << setw(16) << "===============" << left << setw(16) << "===============" << left << setw(15) << "===============" << endl;
   cout << "\t" << left << setw(16) << "Total Score" << right << setw(12) << (yourRoundOne + yourRoundTwo) << setw(5) <<  right << setw(16) << (dateRoundOne + dateRoundTwo) << endl;

   return 0;
}
