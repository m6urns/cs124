/***********************************************************************
* Program:
*    Assignment 25, Loop Design          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program prints out a calander based on user input.
*
*    Estimated:  2.0 hrs   
*    Actual:     4.0 hrs
*      Finding a way to reliably format the output of the for loop
*      took 3 1/2 hours of the total four hours this assignment took. 
************************************************************************/

#include <iostream>
#include <iomanip>

using namespace std;

/**********************************************************************
 * Main takes user input on the number of dasy in the month and the 
 * desired offset, it then prints a calander based on the users input. 
 ***********************************************************************/
int main()
{

   int numDays;
   int offset; 
   int day;

   cout << "Number of days: ";
   cin >> numDays;
   cout << "Offset: ";
   cin >> offset;
   
   do 
   { 
      cout << "  Su  Mo  Tu  We  Th  Fr  Sa" << endl;

      if (offset == 0)
      {
         day = 2;
         cout << setw(6);
      }
      else if (offset == 1)
      { 
         day = 3;
         cout << setw(10);
      }
      else if (offset == 2)
      {
         day = 4;
         cout << setw(14);
      }
      else if (offset == 3)
      {
         day = 5;
         cout << setw(18);
      }
      else if (offset == 4)
      {
         day = 6;
         cout << setw(22);
      }
      else if (offset == 5)
      {
         day = 7;
         cout << setw(26);
      }
      else if (offset == 6)
      {
         day = 1; 
         cout << setw(2);
      }
      else 
      {
         cout << "Offset must be between 0 and 6." << endl;
         cout << "Offset: ";
         cin >> offset;
      }
   } 
   while (offset < 0 && offset <= 6);
   
   for (int calDays = 1; calDays <= numDays; calDays++)
   {
      cout << "  " << setw(2) << calDays;
      ++day;
      if (day == 8)
      {
         cout << "\n";
         day = 1;
      }
   }
   if (day >= 2 && day <= 7)
      cout << "\n";

   return 0;
}
