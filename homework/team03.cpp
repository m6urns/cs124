/***********************************************************************
* Program:
*    Assignment ##, ????          (e.g. Assignment 01, Hello World)  
*    Brother {Grimmett, Sloan, Comeau, Neff, Helfrich}, CS124
* Author:
*    your name
* Summary: 
*    Enter a brief description of your program here!  Please note that if
*    you do not take the time to fill out this block, YOU WILL LOSE POINTS.
*    Before you begin working, estimate the time you think it will
*    take you to do the assignment and include it in this header block.
*    Before you submit the assignment include the actual time it took.
*
*    Estimated:  0.0 hrs   
*    Actual:     0.0 hrs
*      Please describe briefly what was the most difficult part.
************************************************************************/

#include <iostream>
using namespace std;

float getMissleDistance();
float convertKilometersToMiles(float km);
bool shouldFireMissle(float distance);

/**********************************************************************
 * Add text here to describe what the function "main" does. Also don't forget
 * to fill this out with meaningful text or YOU WILL LOSE POINTS.
 ***********************************************************************/
int main()
{

    float theDistance = getMissleDistance();
    float miles = convertKilometerToMiles(theDistance);


    cout << "Super Secret Missle Defense Program" << endl;
    cout << " " << endl;
  
    if shouldFireMissle()
       cout << "\tDistance is " << miles << " miles, fire!" << endl;
    else  
       cout << "\tDistance is " << miles << " miles, hold your fire." << endl;
   
    return 0;
}

float getMissleDistance() 
{
   
   float distance;

   cout << "\tEnter the distance of the missile: ";
   cin >> distance;
      
   return distance;
}

float convertKilometersToMiles(float km) 
{

   float miles;

   miles = km * 0.62137;
 
   return miles;  
}

bool shouldFireMissle(float distance) 
{

   bool shouldFireMissle = convertKilometersToMiles <= 30;
     
   return shouldFireMissle;
}

