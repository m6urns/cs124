/***********************************************************************
* Program:
*    Assignment 26, Files          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program takes a set of values from a file and calculates 
*    the average. 
*
*    Estimated:  0.75 hrs   
*    Actual:     1.0 hrs
*      Error handling was difficult, I'm sure there is a better 
*      way to do it than the way I did but I couldn't figure it
*      out for the life of me.
************************************************************************/

#include <iostream>
#include <fstream>
using namespace std;

void getFilename(char fileName[]);
int countData(char fileName[]);
float readFile(char fileName[], int count);
void display(float average, int count);

/**********************************************************************
 * The function main calls all the working functions and passes 
 * passes information between them.
 ***********************************************************************/
int main()
{
   
   char fileName[256]; 
   getFilename(fileName);
   int count = countData(fileName);
   float average = readFile(fileName, count);

   display(average, count);

   return 0;
}

/**********************************************************************
 * The function getFilename gets the name of the file you wish to
 * calculate an aveg from.  
 ***********************************************************************/
void getFilename(char fileName[])
{
  // char fileName[256];

   cout << "Please enter the filename: ";
   cin >> fileName;
}

/**********************************************************************
 * The function countData counts the number of values in the file
 * and returns that number. I did this because I couldnt figure out
 * how to do display correctly without the value of count.
 ***********************************************************************/
int countData(char fileName[])
{
   ifstream fin(fileName);
   
   int data;  
   int count = 0;   

   while (fin >> data)
   {   
      count++;
   }

   fin.close();
   
   return count;
}

/**********************************************************************
 * The function readFile reads the file a second time but this time 
 * takes the data and returns it avgd.
 ***********************************************************************/
float readFile(char fileName[], int count)
{  
   float average;

   ifstream fin(fileName);
   if (fin.fail())
   {
      cout << "Error reading file " << "\"" << fileName << "\"" << endl;
      return 0;
   } 
   float data;
   float sum = 0;

   if (count == 10) 
   {  
      while (fin >> data)
         sum += data;
   } 
   else if (count != 10)
   {
      cout << "Error reading file " << "\"" << fileName << "\"" << endl; 
      return 0;
   }
   fin.close();
 
   average = (sum / 10);
     
   return average;
}

/**********************************************************************
 * The function display decides the kind of output to display based on
 * the value of count. It also displays the value calculated in
 * readFile
 ***********************************************************************/
void display(float average, int count)
{
   cout.setf(ios::fixed);
   cout.precision(0);
   
   if (count == 10)    
      cout << "Average Grade: " << average << "%" << endl;  
}
