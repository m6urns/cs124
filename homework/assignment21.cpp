/***********************************************************************
* Program:
*    Assignment 21, Function Stubs          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program prints Success. Hopefully indicating that
*    I have properly created stubs and moved 0's around 
*    correctly. 
*
*    Estimated:  0.5 hrs   
*    Actual:     0.5 hrs
*      I accidently was passing two int to main which come to 
*      find out is not supposed to happen. 
************************************************************************/

#include <iostream>
using namespace std;

void display();
int getPeriodicCost(int cost);
int promptDevalue();
int promptInsurance();
int promptParking();
int getUsagecost(int mileage, int cost);
int promptMilage();
int promptGas();
int promptRepairs();
int promptTires();

/**********************************************************************
 * Add text here to describe what the function "main" does. Also don't forget
 * to fill this out with meaningful text or YOU WILL LOSE POINTS.
 ***********************************************************************/
int main()
{
   cout << "Success\n";
   return 0;
}

void display(int costPeriodic, int costUsage)
{

}

int getPeriodicCost(int cost)
{
   int costPeriodic = 0;
   return costPeriodic;
}

int promptDevalue()
{
   int cost = 0;
   return cost;
}

int promptInsurance()
{
   int cost = 0;
   return cost;
}

int promptParking()
{
   int cost = 0;
   return cost;
}

int getUsageCost(int mileage, int cost)
{  
   int costUsage = 0;
   return costUsage;
}

int promptMileage()
{
   int mileage = 0;
   return mileage;
}

int promptGas()
{
   int cost = 0;
   return cost;
}

int promptRepairs()
{
   int cost = 0;
   return cost;
}

int promptTires()
{
   int cost = 0;
   return cost;
}
