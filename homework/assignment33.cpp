/***********************************************************************
* Program:
*    Assignment 33, Pointers          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This progra gets the total cost of a date and tells you
*    how much you lost of your savings.
*
*    Estimated:  0.5 hrs   
*    Actual:     0.25 hrs
*      Using the pointer to decrement the balance was the hardest part 
*      to figure out.
************************************************************************/

#include <iostream>
using namespace std;

float getSamsBalance();
float getSuesBalance();
float getCost();
void displayBalance(float samsBalance, float suesBalance, float cost);

/**********************************************************************
 * The function main passes information between functions within the
 * program. And calls the final function to display the balance
 * of the respective accounts. 
 ***********************************************************************/
int main()
{
   float samsBalance = getSamsBalance();
   float suesBalance = getSuesBalance();

   float cost = getCost();

   displayBalance(samsBalance, suesBalance, cost);

   return 0;
}

/**********************************************************************
 * getSamsBalance get the account balance of sam and returns it for us 
 * to use later.
 ***********************************************************************/
float getSamsBalance()
{
   float samsBalance = 0;

   cout << "What is Sam's balance? ";
   cin >> samsBalance;

   return samsBalance;
}

/**********************************************************************
 * getSuesBalance is an exact copy of the last function it operates 
 * in the same way but gets sues balance.
 ***********************************************************************/
float getSuesBalance()
{
   float suesBalance = 0;
   
   cout << "What is Sue's balance? ";
   cin >> suesBalance;

   return suesBalance;
}

/**********************************************************************
 * getCost prompts the user for how much they spent last night and 
 * totals it together, and returns that total as cost.
 ***********************************************************************/
float getCost()
{
   float dinner = 0;
   float movie = 0;
   float iceCream = 0;

   cout << "Cost of the date:" << endl;
   cout << "\t" << "Dinner:    ";
   cin >> dinner;
   cout << "\t" << "Movie:     ";
   cin >> movie;
   cout << "\t" << "Ice cream: "; 
   cin >> iceCream;

   float cost = (dinner + movie + iceCream);
   
   return cost;
}

/**********************************************************************
 * The function displayBalance displays the final damage to the 
 * bank account of the richer. It uses a pointer to subtract the 
 * cost from the bank account with more money. 
 ***********************************************************************/
void displayBalance(float samsBalance, float suesBalance, float cost)
{
   float *pAccount;

   if (samsBalance > suesBalance)
      pAccount = &samsBalance;
   else 
      pAccount = &suesBalance;

   *pAccount -= cost;   

   cout << "Sam's balance: $" << samsBalance << endl;
   cout << "Sue's balance: $" << suesBalance << endl;
}

