/***********************************************************************
* Program:
*    Assignment 23, Loop Syntax          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program finds the sum of the multiples of a number under 100
*    using counter-controlled loops. I hope adding the ability to define
*    the boundery for calculation with -DBOUND was ok.  
*
*    Estimated:  1.0 hrs   
*    Actual:     0.75 hrs
*      Getting the math for the submation loop was the most difficult 
*      part.
************************************************************************/

#include <iostream>
using namespace std;

int getNumber();
int getBound();
int sumMultiples(int number, int bound);
void returnSum(int sum, int number, int bound);

/**********************************************************************
 * The function main calls the functions that make up the program 
 * in the correct order. 
 ***********************************************************************/
int main()
{
   int number = getNumber();
   int bound = getBound();
   int sum = sumMultiples(number, bound);
   
   returnSum(sum, number, bound);   

   return 0;
}

/***********************************************************************
 * The function getNumber takes the number the user wishes to input 
 * to calculate the value of all the multiples of a number under
 * 100. 
 ***********************************************************************/
int getNumber() 
{
   int number;
   
   cout << "What multiples are we adding? ";
   cin >> number;

   #ifdef DEBUG
      cout << number << endl;
   #endif

      return number;
}

/**********************************************************************
 * The function getBound allows the user to set the bound to which the 
 * sum is calculated. But only if bound is defined at compile time. Just
 * fun. 
 ***********************************************************************/
int getBound()
{
   int bound = 100;
   
   #ifdef BOUND
      cout << "Within what bound would you like to calculate? ";
      cin >> bound;
   #endif

      return bound;
}

/**********************************************************************
 * The function sumMultiples adds together all the multiples of a number 
 * under 100. 
 ***********************************************************************/
int sumMultiples(int number, int bound)
{
   int sum = 0;

   #ifdef DEBUG 
      cout << "We made it this far..." << endl;
   #endif
 
      for (int count = number; count < bound; count += number )
         sum += count;

   #ifdef DEBUG
      cout << sum << endl;
   #endif 

      return sum;
}

/**********************************************************************
 * The function ruturnSum prints the value of the sum that the user
 * inputed in getNumber, and the sumMultiples found. 
 ***********************************************************************/
void returnSum(int sum, int number, int bound)
{
   cout << "The sum of multiples of " << number << " less than " << bound 
      << " are: " << sum << endl;
}      
