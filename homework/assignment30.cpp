/***********************************************************************
* Program:
*    Assignment 30, Array Syntax          (e.g. Assignment 01, Hello World)  
*    Brother Falin, CS124
* Author:
*    Matthew Burns
* Summary: 
*    This program takes ten grades from users and averages them using
*    an array to hold the data.
*
*    Estimated:  1.0 hrs   
*    Actual:     75.0 hrs
*      Passing the array data between the functions was the most time 
*      consuming part.
************************************************************************/

#include <iostream>
using namespace std;

void getGrades(int grades[]);
int averageGrades(int grades[]);
void display(int average);

/**********************************************************************
 * Main initiates and stores the array grades, it then passes this 
 * information as requested. It also calls functions to get user
 *  input and display an average.  
 ***********************************************************************/
int main()
{
   int grades[10];
   getGrades(grades);
   int average = averageGrades(grades);
  
   display(average);
  
   return 0;
}

/**********************************************************************
 * Grades gets user input and passes it into the grade array for the 
 * average grades function to operate on. It also handles errors in
 * input.
 ***********************************************************************/
void getGrades(int grades[])
{
   int number = 1;

   for (int i = 0; i < 10; i++)
   {
      cout << "Grade " << number << ": ";
      cin >> grades[i];
     
      while (grades[i] > 100)
      {
         cout << "Invalid Grade (< 100)" << endl;
         cout << "Grade " << number << ": ";
         cin >> grades[i];
      }
   
      number++;
   } 
}

/**********************************************************************
 * Average grades takes the grades input into the array and calculates 
 * the average of these grades. If the grade is valid it adds the grade
 * to the totalGrades and incrments a counter to divide this number by.
 * If the grade is invalid it is not added to the total and the counter
 * is not incremnted. If the count at the end = 0 all inputs have been 
 * been invalid and the functions returns an indicator. 
 ***********************************************************************/
int averageGrades(int grades[])
{
   int count = 0;
   int throwAway = 0;
   int totalGrades = 0;

   #ifdef DEBUG
      for (int i = 0; i < 10; i++)
   {
      cout << grades[i] << endl;
   }
   #endif

      for (int i = 0; i < 10; i++)
   {
      if (grades[i] < 0)
         throwAway += grades[i];
      else
      { 
         totalGrades += grades[i];
         count++;
      }  
   }

   int average = 0;
   
   if (count == 0)
      average = 101;
   else
   average = (totalGrades / count);

   return average;
}

/**********************************************************************
 * Display displays the average calculated in the averageGrades 
 * function.
 ***********************************************************************/
void display(int average)
{
   if (average == 101)
      cout << "Average Grade: ---%" << endl;
   else 
      cout << "Average Grade: " << average << "%" << endl;
}
